﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace gtalife.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Username = table.Column<string>(nullable: false),
                    Password = table.Column<string>(nullable: true),
                    SocialClubName = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    LastIp = table.Column<string>(nullable: true),
                    Admin = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Weather",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Current = table.Column<int>(nullable: false),
                    Next = table.Column<int>(nullable: false),
                    Duration = table.Column<TimeSpan>(nullable: false),
                    CreatedAt = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Weather", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Characters",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    UserId = table.Column<int>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    Gender = table.Column<bool>(nullable: false),
                    PhoneNumber = table.Column<int>(nullable: true),
                    Money = table.Column<int>(nullable: false),
                    Bank = table.Column<int>(nullable: false),
                    Level = table.Column<int>(nullable: false),
                    Experience = table.Column<int>(nullable: false),
                    Faction = table.Column<Guid>(nullable: true),
                    FacRank = table.Column<int>(nullable: true),
                    Job = table.Column<int>(nullable: true),
                    WantedLevel = table.Column<int>(nullable: false),
                    JailTime = table.Column<int>(nullable: false),
                    PositionX = table.Column<float>(nullable: false),
                    PositionY = table.Column<float>(nullable: false),
                    PositionZ = table.Column<float>(nullable: false),
                    RotationZ = table.Column<float>(nullable: false),
                    Dimension = table.Column<uint>(nullable: false),
                    DrivingLicense = table.Column<bool>(nullable: false),
                    InsideHouseId = table.Column<Guid>(nullable: true),
                    Gang = table.Column<Guid>(nullable: true),
                    GangRank = table.Column<int>(nullable: false),
                    PlayedTime = table.Column<double>(nullable: false),
                    LastLogin = table.Column<DateTime>(nullable: false),
                    CreatedAt = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Characters", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Characters_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CharacterClothes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    CharacterId = table.Column<int>(nullable: true),
                    Slot = table.Column<int>(nullable: false),
                    Drawable = table.Column<int>(nullable: false),
                    Texture = table.Column<int>(nullable: false),
                    IsProp = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CharacterClothes", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CharacterClothes_Characters_CharacterId",
                        column: x => x.CharacterId,
                        principalTable: "Characters",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CharacterContacts",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    CharacterId = table.Column<int>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    Phone = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CharacterContacts", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CharacterContacts_Characters_CharacterId",
                        column: x => x.CharacterId,
                        principalTable: "Characters",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CharacterTrait",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    CharacterId = table.Column<int>(nullable: false),
                    FaceFirst = table.Column<int>(nullable: false),
                    FaceSecond = table.Column<int>(nullable: false),
                    FaceMix = table.Column<float>(nullable: false),
                    SkinFirst = table.Column<int>(nullable: false),
                    SkinSecond = table.Column<int>(nullable: false),
                    SkinMix = table.Column<float>(nullable: false),
                    HairType = table.Column<int>(nullable: false),
                    HairColor = table.Column<int>(nullable: false),
                    HairHighlight = table.Column<int>(nullable: false),
                    EyeColor = table.Column<int>(nullable: false),
                    Eyebrows = table.Column<int>(nullable: false),
                    EyebrowsColor1 = table.Column<int>(nullable: false),
                    EyebrowsColor2 = table.Column<int>(nullable: false),
                    Beard = table.Column<int>(nullable: true),
                    BeardColor = table.Column<int>(nullable: true),
                    Makeup = table.Column<int>(nullable: true),
                    MakeupColor = table.Column<int>(nullable: true),
                    Lipstick = table.Column<int>(nullable: true),
                    LipstickColor = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CharacterTrait", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CharacterTrait_Characters_CharacterId",
                        column: x => x.CharacterId,
                        principalTable: "Characters",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CharacterVehicles",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    CharacterId = table.Column<int>(nullable: true),
                    Price = table.Column<int>(nullable: false),
                    Model = table.Column<uint>(nullable: false),
                    Color1 = table.Column<int>(nullable: false),
                    Color2 = table.Column<int>(nullable: false),
                    Fuel = table.Column<int>(nullable: false),
                    PositionX = table.Column<float>(nullable: false),
                    PositionY = table.Column<float>(nullable: false),
                    PositionZ = table.Column<float>(nullable: false),
                    RotationX = table.Column<float>(nullable: false),
                    RotationY = table.Column<float>(nullable: false),
                    RotationZ = table.Column<float>(nullable: false),
                    CreatedAt = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CharacterVehicles", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CharacterVehicles_Characters_CharacterId",
                        column: x => x.CharacterId,
                        principalTable: "Characters",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CharacterWeapons",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    CharacterId = table.Column<int>(nullable: true),
                    Weapon = table.Column<int>(nullable: false),
                    Ammo = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CharacterWeapons", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CharacterWeapons_Characters_CharacterId",
                        column: x => x.CharacterId,
                        principalTable: "Characters",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_CharacterClothes_CharacterId",
                table: "CharacterClothes",
                column: "CharacterId");

            migrationBuilder.CreateIndex(
                name: "IX_CharacterContacts_CharacterId",
                table: "CharacterContacts",
                column: "CharacterId");

            migrationBuilder.CreateIndex(
                name: "IX_Characters_UserId",
                table: "Characters",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_CharacterTrait_CharacterId",
                table: "CharacterTrait",
                column: "CharacterId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_CharacterVehicles_CharacterId",
                table: "CharacterVehicles",
                column: "CharacterId");

            migrationBuilder.CreateIndex(
                name: "IX_CharacterWeapons_CharacterId",
                table: "CharacterWeapons",
                column: "CharacterId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CharacterClothes");

            migrationBuilder.DropTable(
                name: "CharacterContacts");

            migrationBuilder.DropTable(
                name: "CharacterTrait");

            migrationBuilder.DropTable(
                name: "CharacterVehicles");

            migrationBuilder.DropTable(
                name: "CharacterWeapons");

            migrationBuilder.DropTable(
                name: "Weather");

            migrationBuilder.DropTable(
                name: "Characters");

            migrationBuilder.DropTable(
                name: "Users");
        }
    }
}
