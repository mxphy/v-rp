﻿using GTANetworkAPI;
using gtalife.src.Player.Utils;

namespace gtalife.src.Gameplay
{
    class VehicleMenu : Script
    {
        [ServerEvent(Event.PlayerExitVehicle)]
        public void OnPlayerExitVehicle(Client player, Vehicle vehicle)
        {
            if (NAPI.Vehicle.GetVehicleLocked(vehicle))
            {
                NAPI.Vehicle.SetVehicleLocked(vehicle, false);
            }
        }

        [RemoteEvent("open_vehicle_hood")]
        public void OpenVehicleHood(Client sender, object[] arguments)
        {
            NAPI.Vehicle.SetVehicleDoorState(sender.Vehicle, 4, !NAPI.Vehicle.GetVehicleDoorState(sender.Vehicle, 4));
            sender.sendChatAction((NAPI.Vehicle.GetVehicleDoorState(sender.Vehicle, 4)) ? "abrió el capó del vehículo." : "cerró el capo del vehículo.");
        }

        [RemoteEvent("open_vehicle_trunk")]
        public void OpenVehicleTrunk(Client sender, object[] arguments)
        {
            NAPI.Vehicle.SetVehicleDoorState(sender.Vehicle, 5, !NAPI.Vehicle.GetVehicleDoorState(sender.Vehicle, 5));
            sender.sendChatAction((NAPI.Vehicle.GetVehicleDoorState(sender.Vehicle, 5)) ? "abrió el maletero del vehículo." : "cerró el maletero del vehículo.");
        }

        [RemoteEvent("open_vehicle_doors")]
        public void OpenVehicleDoors(Client sender, object[] arguments)
        {
            bool state = NAPI.Vehicle.GetVehicleDoorState(sender.Vehicle, 0);

            NAPI.Vehicle.SetVehicleDoorState(sender.Vehicle, 0, !state);
            NAPI.Vehicle.SetVehicleDoorState(sender.Vehicle, 1, !state);
            NAPI.Vehicle.SetVehicleDoorState(sender.Vehicle, 2, !state);
            NAPI.Vehicle.SetVehicleDoorState(sender.Vehicle, 3, !state);

            sender.sendChatAction((!state) ? "abrió las puertas del vehículo." : "cerró las puertas del vehículo.");
        }

        [RemoteEvent("on_player_toggle_vehicle_lock")]
        public void OnPlayerToggleVehicleLock(Client sender, object[] arguments)
        {
            bool state = NAPI.Vehicle.GetVehicleLocked(sender.Vehicle);

            NAPI.Vehicle.SetVehicleLocked(sender.Vehicle, !state);

            sender.sendChatAction((state) ? "desbloqueaste las puertas del vehículo." : "bloqueaste las puertas del vehículo.");
        }

        [Command("v")]
        public void VMenu(Client sender)
        {
            if (!sender.IsInVehicle)
                NAPI.Chat.SendChatMessageToPlayer(sender, $"~r~ERROR: ~s~Usted no está en un vehículo.");
            else if (sender.VehicleSeat != -1)
                NAPI.Chat.SendChatMessageToPlayer(sender, $"~r~ERROR: ~s~Tienes que ser el conductor.");
            else if (Dealership.Main.IsADealershipVehicle(sender.Vehicle))
                NAPI.Chat.SendChatMessageToPlayer(sender, $"~r~ERROR: ~s~No puedes hacer eso en este vehículo.");
            else
                NAPI.ClientEvent.TriggerClientEvent(sender, "open_vehicle_menu");
        }
    }
}
