﻿using GTANetworkAPI;
using gtalife.src.Admin;
using System.IO;
using System.Linq;

namespace gtalife.src.Gameplay.Dealership
{
    class Commands : Script
    {
        [Command("concesioncmds", Alias = "ccmds")]
        public void CommandsCommand(Client player)
        {
            if (!player.IsAdmin())
            {
                player.SendChatMessage("~r~ERROR: ~w~No tienes permison.");
                return;
            }

            NAPI.Chat.SendChatMessageToPlayer(player, "!{#a5f413}~~~~~~~~~~~ Comandos Concesionario ~~~~~~~~~~~");
            NAPI.Chat.SendChatMessageToPlayer(player, "* /crearconcesion - /borrarconcesion - /concesionvehiculo - /borrarconcesionvehiculo - /concesionspawn");
            NAPI.Chat.SendChatMessageToPlayer(player, "!{#a5f413}~~~~~~~~~~~ Comandos Concesionario  ~~~~~~~~~~~");
        }

        [Command("crearconcesion", GreedyArg = true)]
        public void CMD_CreateDealership(Client player, string name)
        {
            if (!player.IsAdmin())
            {
                player.SendChatMessage("~r~ERROR: ~w~No tienes permison.");
                return;
            }

            Dealership new_dealership = new Dealership(Main.GetGuid(), player.Position, name);
            new_dealership.Save();

            Main.Dealerships.Add(new_dealership);
            NAPI.Chat.SendChatMessageToPlayer(player, $"~g~EXITO: ~s~Creaste un concesionario con nombre ~y~{name}~s~.");
        }

        [Command("borrarconcesion")]
        public void CMD_RemoveDealership(Client player)
        {
            if (!player.IsAdmin())
            {
                player.SendChatMessage("~r~ERROR: ~w~No tienes permison.");
                return;
            }

            if (!player.HasData("DealershipMarker_ID"))
            {
                player.SendChatMessage("~r~ERROR: ~w~Párese en el marcador de entrada de la concesionario que desea borrar.");
                return;
            }

            Dealership dealership = Main.Dealerships.FirstOrDefault(h => h.ID == player.GetData("DealershipMarker_ID"));
            if (dealership == null) return;

            dealership.Destroy();
            Main.Dealerships.Remove(dealership);

            string dealership_file = Main.DEALERSHIP_SAVE_DIR + Path.DirectorySeparatorChar + dealership.ID + ".json";
            if (File.Exists(dealership_file)) File.Delete(dealership_file);
            NAPI.Chat.SendChatMessageToPlayer(player, $"~g~EXITO: ~s~Borraste la concesionario con nombre ~y~{dealership.Name}~s~.");
        }

        [Command("concesionvehiculo", "~y~USO: ~w~/concesionvehiculo [precio]")]
        public void CMD_AddDealershipVehicle(Client sender, int price)
        {
            if (!sender.IsInVehicle)
                NAPI.Chat.SendChatMessageToPlayer(sender, $"~r~ERROR: ~s~Necesitas estar en un vehículo.");
            else if (price < 1)
                NAPI.Chat.SendChatMessageToPlayer(sender, $"~r~ERROR: ~s~Precio invalido.");
            else
            {
                foreach (Dealership dealership in Main.Dealerships)
                {
                    if (dealership.Position.DistanceTo(sender.Position) < 40f)
                    {
                        if (dealership.VehicleSpawn.X == 0.0f && dealership.VehicleSpawn.Y == 0.0f)
                        {
                            NAPI.Chat.SendChatMessageToPlayer(sender, "~r~ERROR: ~w~Primero debe configurar el spawn del vehículo. (/concesionspawn)");
                            return;
                        }

                        DealershipVehicle new_vehicle = new DealershipVehicle(sender.Vehicle.Model, sender.Vehicle.PrimaryColor, sender.Vehicle.SecondaryColor, sender.Vehicle.Position, sender.Vehicle.Rotation, price);
                        dealership.Vehicles.Add(new_vehicle);

                        sender.Vehicle.Delete();
                        sender.Position.Z += 0.5f;

                        new_vehicle.Create();
                        dealership.Save();                        
                        
                        NAPI.Chat.SendChatMessageToPlayer(sender, $"~g~EXITO: ~s~Creaste un {sender.Vehicle.DisplayName} para a concesionario ~y~{dealership.Name}~s~.");
                        return;
                    }
                }

                NAPI.Chat.SendChatMessageToPlayer(sender, $"~r~ERROR: ~s~No estás cerca de un concesionario.");
            }
        }

        [Command("borrarconcesionvehiculo")]
        public void CMD_RemoveDealershipVehicle(Client sender)
        {
            if (!sender.IsInVehicle)
                NAPI.Chat.SendChatMessageToPlayer(sender, $"~r~ERROR: ~s~Necesitas estar en un vehículo.");            
            else
            {
                foreach (Dealership dealership in Main.Dealerships)
                {
                    foreach (DealershipVehicle vehicle in dealership.Vehicles)
                    {
                        if (sender.Vehicle == vehicle.Vehicle)
                        {
                            vehicle.Destroy();
                            dealership.Vehicles.Remove(vehicle);
                            dealership.Save();

                            NAPI.Chat.SendChatMessageToPlayer(sender, $"~g~EXITO: ~s~Borraste o vehiculo de la concesionario ~y~{dealership.Name}~s~.");
                            return;
                        }
                    }
                }

                NAPI.Chat.SendChatMessageToPlayer(sender, $"~r~ERROR: ~s~No estás cerca de un concesionario.");
            }
        }

        [Command("concesionspawn")]
        public void CMD_DealershipVehicleSpawn(Client sender)
        {
            if (!sender.IsInVehicle)
                NAPI.Chat.SendChatMessageToPlayer(sender, $"~r~ERROR: ~s~Necesitas estar en un vehículo.");
            else
            {
                var distance = 40f;
                foreach (Dealership dealership in Main.Dealerships)
                {
                    if (dealership.Position.DistanceTo(sender.Position) < distance)
                    {
                        distance = dealership.Position.DistanceTo(sender.Position);
                        dealership.SetSpawn(sender.Vehicle.Position);
                        NAPI.Chat.SendChatMessageToPlayer(sender, $"~g~EXITO: ~s~Usted has configurado el spawn para la concesionario ~y~{dealership.Name}~s~.");
                        return;
                    }
                }

                NAPI.Chat.SendChatMessageToPlayer(sender, $"~r~ERROR: ~s~No estás cerca de un concesionario.");
            }
        }
    }
}
