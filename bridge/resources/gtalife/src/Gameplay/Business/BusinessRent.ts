﻿/// <reference path='../../../types-gt-mp/index.d.ts' />

var business_show_text = false;

API.onServerEventTrigger.connect((event_name: string, args: System.Array<any>) => {
    switch (event_name) {
        case "BusinessRent_ShowText":
            business_show_text = args[0];
            break;
    }
});

API.onKeyDown.connect((sender: any, e: System.Windows.Forms.KeyEventArgs) => {
    if (API.isChatOpen()) return;

    if (e.KeyCode === Keys.E) {
        if (business_show_text == true) {
            API.triggerServerEvent("BusinessRentVehicle");
        }
    }
});

API.onUpdate.connect(() => {
    if (business_show_text == true) API.displaySubtitle("Presione ~y~E ~w~para rentar.", 100);
});