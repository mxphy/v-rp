var is_on_job_marker = false;

mp.events.add('OnEnterJobMark', () => {
    is_on_job_marker = true;
});

mp.events.add('OnLeaveJobMark', () => {
    is_on_job_marker = false;
});

// E
mp.keys.bind(0x45, false, function() {
    if (mp.gui.cursor.visible) return;
    
    if (is_on_job_marker) mp.events.callRemote("JobInteract");
});
