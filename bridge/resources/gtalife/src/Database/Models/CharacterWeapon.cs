﻿namespace gtalife.src.Database.Models
{
    public class CharacterWeapon
    {
        public int Id { get; set; }

        public virtual Character Character { get; set; }

        public int Weapon { get; set; }

        public int Ammo { get; set; }
    }
}
