﻿using GTANetworkAPI;
using gtalife.src.Player.Inventory.Classes;
using gtalife.src.Player.Inventory.Interfaces;
using gtalife.src.Player.Utils;

namespace gtalife.src.Player.Inventory.ItemTypes
{
    public class JerrycanItem : BaseItem, IDroppable
    {
        public WorldModel DropModel { get; set; }

        public JerrycanItem(string name, string description, int stackSize, WorldModel dropModel) : base(name, description, stackSize)
        {
            DropModel = dropModel;
        }

        public override bool Use(Client player)
        {
            if (!player.IsInVehicle)
            {
                player.SendChatMessage("~r~ERROR: ~w~Usted no está en un vehículo.");
                return false;
            }
            else if (player.Vehicle.EngineStatus)
            {
                player.SendChatMessage("~r~ERROR: ~w~Apaga el motor primero.");
                return false;
            }
            /*else if (player.Vehicle.FuelLevel >= 100) // deprecated
            {
                player.SendChatMessage("~r~ERROR: ~w~El combustible está lleno.");
                return false;
            }*/

            //player.Vehicle.fuelLevel = (player.vehicle.fuelLevel + 20f > 100f) ? 100f : player.vehicle.fuelLevel + 20f; // deprecated
            player.sendChatAction("llena el tanque del vehículo con un galón.");
            return true;
        }
    }
}
