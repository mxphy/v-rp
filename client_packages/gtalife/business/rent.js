let business_show_text = false;

mp.events.add('BusinessRent_ShowText', (args) => {
    business_show_text = args;
});

// E key
mp.keys.bind(0x45, false, () => {
    if (mp.gui.cursor.visible) return;
    
    if (business_show_text == true) mp.events.callRemote("BusinessRentVehicle");
});
