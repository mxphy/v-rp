﻿using GTANetworkAPI;
using gtalife.src.Player.Inventory.Classes;
using gtalife.src.Player.Inventory.Interfaces;

namespace gtalife.src.Player.Inventory.ItemTypes
{
    public class WeaponItem : BaseItem, IDroppable
    {
        public WeaponHash Hash { get; set; }
        public WorldModel DropModel { get; set; }

        public WeaponItem(string name, string description, int stackSize, WeaponHash hash, WorldModel dropModel) : base(name, description, stackSize)
        {
            Hash = hash;
            DropModel = dropModel;
        }

        public override bool Use(Client player)
        {
            player.SendChatMessage($"~y~INFO: ~w~Arma recibida. ({Hash})");
            player.GiveWeapon(Hash, 9999);
            return true;
        }
    }
}
