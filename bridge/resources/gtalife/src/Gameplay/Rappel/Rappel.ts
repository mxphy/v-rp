﻿/// <reference path='../../../types-gt-mp/index.d.ts' />

var rappelVehicles = [-1660661558, 353883353, 837858166]; // maverick, emergency maverick, annihilator
var rappelMinHeight = 15.0;
var rappelMaxHeight = 45.0;

API.onServerEventTrigger.connect((eventName: string, args: System.Array<any>) => {
    switch (eventName) {
        case "Rappel":
            if (!API.isPlayerInAnyVehicle(API.getLocalPlayer())) {
                API.SendChatMessage("~r~ERROR: ~w~Usted no está en un helicóptero.");
                return;
            }

            var vehicle = API.getPlayerVehicle(API.getLocalPlayer());
            if (rappelVehicles.indexOf(API.getEntityModel(vehicle)) == -1) {
                API.SendChatMessage("~r~ERROR: ~w~No puedes rapel deste vehículo.");
                return;
            }

            var seat = API.getPlayerVehicleSeat(API.getLocalPlayer());
            if (seat < 1) {
                API.SendChatMessage("~r~ERROR: ~w~No puedes rapel desde este asiento.");
                return;
            }

            if (API.returnNative("GET_ENTITY_SPEED", 7, vehicle) > 10.0) {
                API.SendChatMessage("~r~ERROR: ~w~El helicóptero necesita disminuir la velocidad.");
                return;
            }

            var taskStatus = API.returnNative("GET_SCRIPT_TASK_STATUS", 0, API.getLocalPlayer(), -275944640);
            if (taskStatus == 0 || taskStatus == 1) {
                API.SendChatMessage("~r~ERROR: ~w~Ya estás haciendo rappel.");
                return;
            }

            var entityHeight = API.returnNative("GET_ENTITY_HEIGHT_ABOVE_GROUND", 7, vehicle);
            if (entityHeight < rappelMinHeight || entityHeight > rappelMaxHeight) {
                API.SendChatMessage("~r~ERROR: ~w~Estás demasiado bajo o demasiado alto para hacer rappel.");
                return;
            }

            if (!API.isEntityUpright(vehicle, 15.0)) {
                API.SendChatMessage("~r~ERROR: ~w~El helicóptero necesita estabilizarse.");
                return;
            }

            if (API.isEntityUpsidedown(vehicle)) {
                API.SendChatMessage("~r~ERROR: ~w~No puedes rapel desde un helicóptero boca abajo.");
                return;
            }

            API.triggerServerEvent("RappelFromHelicopter");
            break;        
    }
});

/*API.onKeyDown.connect(function (e, key) {
    if (key.KeyCode == Keys.X) {
        if (API.isChatOpen() || !API.isPlayerInAnyVehicle(API.getLocalPlayer())) return;

        var vehicle = API.getPlayerVehicle(API.getLocalPlayer());
        if (rappelVehicles.indexOf(API.getEntityModel(vehicle)) == -1) return;

        var seat = API.getPlayerVehicleSeat(API.getLocalPlayer());
        if (seat < 1) {
            API.SendChatMessage("~r~ERROR: ~w~No puedes rapel desde este asiento.");
            return;
        }

        if (API.returnNative("GET_ENTITY_SPEED", 7, vehicle) > 10.0) {
            API.SendChatMessage("~r~ERROR: ~w~El helicóptero necesita disminuir la velocidad.");
            return;
        }

        var taskStatus = API.returnNative("GET_SCRIPT_TASK_STATUS", 0, API.getLocalPlayer(), -275944640);
        if (taskStatus == 0 || taskStatus == 1) {
            API.SendChatMessage("~r~ERROR: ~w~Ya estás haciendo rappel.");
            return;
        }

        var entityHeight = API.returnNative("GET_ENTITY_HEIGHT_ABOVE_GROUND", 7, vehicle);
        if (entityHeight < rappelMinHeight || entityHeight > rappelMaxHeight) {
            API.SendChatMessage("~r~ERROR: ~w~Estás demasiado bajo o demasiado alto para hacer rappel.");
            return;
        }

        if (!API.isEntityUpright(vehicle, 15.0)) {
            API.SendChatMessage("~r~ERROR: ~w~El helicóptero necesita estabilizarse.");
            return;
        }

        if (API.isEntityUpsidedown(vehicle)) {
            API.SendChatMessage("~r~ERROR: ~w~No puedes rapel desde un helicóptero boca abajo.");
            return;
        }

        API.triggerServerEvent("RappelFromHelicopter");
    }
});*/
