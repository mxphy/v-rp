﻿using System;
using System.Collections.Generic;

namespace gtalife.src.Database.Models
{
    public class Character
    {
        public int Id { get; set; }

        public virtual User User { get; set; }

        public string Name { get; set; }

        public bool Gender { get; set; }

        public int? PhoneNumber { get; set; }

        public int Money { get; set; }

        public int Bank { get; set; }

        public int Level { get; set; }

        public int Experience { get; set; }

        public Guid? Faction { get; set; }

        public int? FacRank { get; set; }

        public int? Job { get; set; }

        public int WantedLevel { get; set; }

        public int JailTime { get; set; }

        public float PositionX { get; set; }

        public float PositionY { get; set; }

        public float PositionZ { get; set; }

        public float RotationZ { get; set; }

        public uint Dimension { get; set; }

        public bool DrivingLicense { get; set; }

        public Guid? InsideHouseId { get; set; }

        public Guid? Gang { get; set; }

        public int GangRank { get; set; }

        public double PlayedTime { get; set; }

        public DateTime LastLogin { get; set; }

        public DateTime CreatedAt { get; set; }

        public virtual CharacterTrait Trait { get; set; }

        public virtual ICollection<CharacterClothes> Clothes { get; set; }

        public virtual ICollection<CharacterContact> Contacts { get; set; }

        public virtual ICollection<CharacterVehicle> Vehicles { get; set; }

        public virtual ICollection<CharacterWeapon> Weapons { get; set; }
    }
}
