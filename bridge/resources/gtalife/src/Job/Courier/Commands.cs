﻿using System;
using System.IO;
using System.Linq;
using GTANetworkAPI;
using gtalife.src.Admin;

namespace gtalife.src.Job.Courier
{
    class Commands : Script
    {
        [Command("couriercmds")]
        public void CommandsCommand(Client player)
        {
            NAPI.Chat.SendChatMessageToPlayer(player, "!{#a5f413}~~~~~~~~~~~ Comandos Courier ~~~~~~~~~~~");
            NAPI.Chat.SendChatMessageToPlayer(player, "* /cfactory - /cbuyer - /setfactorystock - /setbuyerstock - /setbuyermaxstock - /rfactory - /rbuyer");
            NAPI.Chat.SendChatMessageToPlayer(player, "!{#a5f413}~~~~~~~~~~~ Comandos Courier ~~~~~~~~~~~");
        }

        [Command("cfactory")]
        public void CMD_CreateFactory(Client player, ProductType type)
        {
            if (!player.IsAdmin())
            {
                player.SendChatMessage("~r~ERROR: ~w~Solo los administradores pueden usar este comando.");
                return;
            }

            // generate id
            Guid factoryID;

            do
            {
                factoryID = Guid.NewGuid();
            } while (Main.Factories.FirstOrDefault(f => f.ID == factoryID) != null);

            // create factory & save
            Factory newFactory = new Factory(factoryID, player.Position, type);
            Main.Factories.Add(newFactory);

            newFactory.CreateEntities();
            newFactory.Save();
        }

        [Command("cbuyer")]
        public void CMD_CreateBuyer(Client player, ProductType type, int maxStock)
        {
            if (!player.IsAdmin())
            {
                player.SendChatMessage("~r~ERROR: ~w~Solo los administradores pueden usar este comando.");
                return;
            }

            // generate id
            Guid buyerID;

            do
            {
                buyerID = Guid.NewGuid();
            } while (Main.Buyers.FirstOrDefault(b => b.ID == buyerID) != null);

            // create buyer & save
            Buyer newBuyer = new Buyer(buyerID, player.Position, type, maxStock: maxStock);
            Main.Buyers.Add(newBuyer);

            newBuyer.CreateEntities();
            newBuyer.Save();
        }

        [Command("setfactorystock")]
        public void CMD_SetFactoryStock(Client player, int newStock)
        {
            if (!player.IsAdmin())
            {
                player.SendChatMessage("~r~ERROR: ~w~Solo los administradores pueden usar este comando.");
                return;
            }

            if (!player.HasData("Courier_MarkerID")) return;

            Factory factory = Main.Factories.FirstOrDefault(f => f.ID == player.GetData("Courier_MarkerID"));
            if (factory == null)
            {
                player.SendChatMessage("~r~ERROR: ~w~No estás en el marcador de una fábrica.");
                return;
            }

            factory.Stock = newStock;
            factory.Save(true);
        }

        [Command("setbuyerstock")]
        public void CMD_SetBuyerStock(Client player, int newStock)
        {
            if (!player.IsAdmin())
            {
                player.SendChatMessage("~r~ERROR: ~w~Solo los administradores pueden usar este comando.");
                return;
            }

            if (!player.HasData("Courier_MarkerID")) return;

            Buyer buyer = Main.Buyers.FirstOrDefault(b => b.ID == player.GetData("Courier_MarkerID"));
            if (buyer == null)
            {
                player.SendChatMessage("~r~ERROR: ~w~No estás en un marcador de un comprador.");
                return;
            }

            buyer.Stock = newStock;
            buyer.Save(true);
        }

        [Command("setbuyermaxstock")]
        public void CMD_SetBuyerMaxStock(Client player, int newMaxStock)
        {
            if (!player.IsAdmin())
            {
                player.SendChatMessage("~r~ERROR: ~w~Solo los administradores pueden usar este comando.");
                return;
            }

            if (!player.HasData("Courier_MarkerID")) return;

            Buyer buyer = Main.Buyers.FirstOrDefault(b => b.ID == player.GetData("Courier_MarkerID"));
            if (buyer == null)
            {
                player.SendChatMessage("~r~ERROR: ~w~No estás en un marcador de un comprador.");
                return;
            }

            buyer.MaxStock = newMaxStock;
            buyer.Save(true);
        }

        [Command("rfactory")]
        public void CMD_RemoveFactory(Client player)
        {
            if (!player.IsAdmin())
            {
                player.SendChatMessage("~r~ERROR: ~w~Solo los administradores pueden usar este comando.");
                return;
            }

            if (!player.HasData("Courier_MarkerID")) return;

            Factory factory = Main.Factories.FirstOrDefault(f => f.ID == player.GetData("Courier_MarkerID"));
            if (factory == null)
            {
                player.SendChatMessage("~r~ERROR: ~w~No estás en el marcador de una fábrica.");
                return;
            }

            factory.DeleteEntities();
            Main.Factories.Remove(factory);
            if (File.Exists(factory.FilePath)) File.Delete(factory.FilePath);
        }

        [Command("rbuyer")]
        public void CMD_RemoveBuyer(Client player)
        {
            if (!player.IsAdmin())
            {
                player.SendChatMessage("~r~ERROR: ~w~Solo los administradores pueden usar este comando.");
                return;
            }

            if (!player.HasData("Courier_MarkerID")) return;

            Buyer buyer = Main.Buyers.FirstOrDefault(b => b.ID == player.GetData("Courier_MarkerID"));
            if (buyer == null)
            {
                player.SendChatMessage("~r~ERROR: ~w~No estás en un marcador de un comprador.");
                return;
            }

            buyer.DeleteEntities();
            Main.Buyers.Remove(buyer);
            if (File.Exists(buyer.FilePath)) File.Delete(buyer.FilePath);
        }
    }
}
