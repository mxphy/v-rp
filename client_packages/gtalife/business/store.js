const NativeUI = require("nativeui");
const Menu = NativeUI.Menu;
const UIMenuItem = NativeUI.UIMenuItem;
const Point = NativeUI.Point;

let business_menu = null;

let products = [
    { name: "Botiquín", price: "50", description: "Medicamentos para recuperar tu salud." },
    { name: "Teléfono", price: "750", description: "Utilizado para llamar a otros jugadores." },
    { name: "Bidón", price: "125", description: "Un recipiente hermético utilizado para contener, transportar y almacenar líquidos." },
    { name: "Radio", price: "800", description: "Un dispositivo electrónico que recibe ondas de radio y convierte la información que llevan en una forma utilizable." },
];

mp.events.add('BusinessStore', () => {
    if (business_menu == null) {
        business_menu = new Menu(" ", "Seleccione un item", new Point(activeResolution.x - 150, 50), "shopui_title_conveniencestore", "shopui_title_conveniencestore");

        business_menu.ItemSelect.on((item, index) => {
            mp.events.callRemote("BusinessBuyItem", JSON.stringify(products[index]));
        });
    }

    // business main menu
    business_menu.Clear();

    for (let i = 0, len = products.length; i < len; i++) {
        let temp_item = new UIMenuItem(`${products[i].name}`, `${products[i].description}`);
        temp_item.SetRightLabel(`$${products[i].price}`);
        business_menu.AddItem(temp_item);
    }

    business_menu.Visible = true;
});
