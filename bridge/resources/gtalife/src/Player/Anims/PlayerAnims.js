﻿var anim_menu = null;
var anim_playing = false;

API.onServerEventTrigger.connect(function (event_name, args) {
    switch (event_name) {
        case "ReceiveAnims":
            if (anim_menu == null) {
                var data = JSON.parse(args[0]);

                anim_menu = API.createMenu("Anims", "~b~Seleccione una anim.", 0, 0, 6);
                for (var i = 0; i < data.length; i++) anim_menu.AddItem(API.createMenuItem(data[i], ""));

                anim_menu.OnItemSelect.connect(function (menu, item, index) {
                    API.triggerServerEvent("PlayAnim", index);
                });

                anim_menu.RefreshIndex();
                anim_menu.Visible = true;
            }
            break;
        case "SetAnimPlaying":
            anim_playing = args[0];
            break;
    }
});

API.onKeyDown.connect(function (e, key) {
    if (API.isChatOpen()) return;

    if (key.KeyCode == Keys.F6) {
        if (anim_menu == null) {
            API.triggerServerEvent("RequestAnims");
        } else {
            anim_menu.Visible = !anim_menu.Visible;
        }
    }
    else if ((key.KeyCode == Keys.W || key.KeyCode == Keys.A || key.KeyCode == Keys.S || key.KeyCode == Keys.D) && anim_playing) {
        API.triggerServerEvent("StopPlayerAnim");
    }
});
