﻿using GTANetworkAPI;
using gtalife.src.Flags;
using gtalife.src.Player.Inventory.Classes;
using gtalife.src.Player.Inventory.Interfaces;
using gtalife.src.Player.Utils;

namespace gtalife.src.Player.Inventory.ItemTypes
{
    public class DrinkItem : BaseItem, IDroppable
    {
        public WorldModel DropModel { get; set; }

        public DrinkItem(string name, string description, int stackSize, WorldModel dropModel) : base(name, description, stackSize)
        {
            DropModel = dropModel;
        }

        public override bool Use(Client player)
        {
            var name = Name.ToLower();
            player.sendChatAction($"bebe un {name}.");

            var bottle = NAPI.Object.CreateObject(NAPI.Util.GetHashKey(DropModel.ModelName), player.Position, new Vector3(0, 0, 0));
            if (player.IsInVehicle)
            {
                NAPI.Entity.AttachEntityToEntity(bottle, player, "SKEL_L_Hand", new Vector3(0.14, -0.04, 0.04), new Vector3(0, 90.0, 90.0));
                player.PlayAnimation("amb@code_human_in_car_mp_actions@drink_bottle@std@ds@base", "idle_a", (int)(AnimationFlags.Loop | AnimationFlags.OnlyAnimateUpperBody | AnimationFlags.AllowPlayerControl));
            }
            else
            {
                NAPI.Entity.AttachEntityToEntity(bottle, player, "SKEL_R_Hand", new Vector3(0.14, -0.04, -0.04), new Vector3(0, 90.0, 90.0));
                player.PlayAnimation("amb@world_human_drinking@coffee@male@idle_a", "idle_a", (int)(AnimationFlags.Loop | AnimationFlags.OnlyAnimateUpperBody | AnimationFlags.AllowPlayerControl));
            }

            NAPI.Task.Run(() =>
            {
                NAPI.Entity.DeleteEntity(bottle);
                player.StopAnimation();
            }, delayTime: 6000);
            return true;
        }
    }
}
