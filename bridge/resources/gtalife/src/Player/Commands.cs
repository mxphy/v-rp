﻿using GTANetworkAPI;
using gtalife.src.Admin;
using gtalife.src.Managers;
using gtalife.src.Player.Utils;
using System;
using System.Linq;

namespace gtalife.src.Player
{
    class Commands : Script
    {
        [Command("commands", Alias = "cmds")]
        public void CommandsCommand(Client player)
        {
            NAPI.Chat.SendChatMessageToPlayer(player, "!{#a5f413}~~~~~~~~~~~ Comandos Generales ~~~~~~~~~~~");
            NAPI.Chat.SendChatMessageToPlayer(player, "* /me - /do - /(g)shout - /s(low) - /pagar - /n - /canaldudas - /cuenta");
            NAPI.Chat.SendChatMessageToPlayer(player, "* /b - /llenar - /licencias - /id - /rappel - /re - /taxi");
            NAPI.Chat.SendChatMessageToPlayer(player, "* /ayudatrabajo - /ayudafaccion - /ayudatelefono - /ayudagang");
            NAPI.Chat.SendChatMessageToPlayer(player, "!{#a5f413}~~~~~~~~~~~ Comandos Generales ~~~~~~~~~~~");
        }

        [Command("id", GreedyArg = true)]
        public void CMD_FindPlayer(Client sender, string idOrName)
        {
            var target = PlayeridManager.FindPlayer(idOrName);
            if (target == null) sender.SendChatMessage("~r~ERROR: ~w~Demasiados o ningún jugador encontrado.");
            else sender.SendChatMessage($"~y~JUGADOR: ~w~{target.Name} - ~y~ID: ~w~{PlayeridManager.GetPlayerId(target)}");
        }

        [Command("re", GreedyArg = true)]
        public void CMD_Report(Client sender, string idOrName, string reason)
        {
            var target = PlayeridManager.FindPlayer(idOrName);
            if (target == null) sender.SendChatMessage("~r~ERROR: ~w~Demasiados o ningún jugador encontrado.");
            else if (sender.HasData("HasReported")) sender.SendChatMessage("~r~ERROR: ~w~Usted ya informó, espere una respuesta.");
            else
            {
                sender.SetData("HasReported", true);
                sender.SendChatMessage($"~y~INFO: ~w~Has informado sobre {target.Name}. Razón: {reason}");

                foreach (var player in NAPI.Pools.GetAllPlayers())
                {
                    if (player == sender) continue;

                    if (player.IsAdmin())
                    {
                        player.SendChatMessage($"~y~INFO: ~w~{sender.Name} has informado sobre {target.Name}. Razón: {reason}");
                        player.SendChatMessage("~c~* (( /ar - /rr ))");
                    }
                }
            }
        }

        [Command("ayudagang")]
        public void HelpGang(Client sender)
        {
            sender.SendChatMessage("~y~INFO: ~w~/creargang - /editargangrank - /editarrank - /borrargang - /reclutar - /gdespedir - /dejargang - (/r)adio - /f");
        }

        [Command("radio", Alias = "r", GreedyArg = true)]
        public void RadioCommand(Client player, string message)
        {
            if (player.GetFactionID() != null) Faction.Main.SendPlayerFactionMessage(player, message);
            else if (player.IsGangMember()) Gameplay.Gang.Commands.SendPlayerRadioMessage(player, message);
            else NAPI.Chat.SendChatMessageToPlayer(player, "~r~ERROR: ~w~No tienes permiso.");
        }

        [Command("f", GreedyArg = true)]
        public void OOCRadioCommand(Client player, string message)
        {
            if (player.GetFactionID() != null) Faction.Main.SendPlayerFactionMessage(player, message, false);
            else if (player.IsGangMember()) Gameplay.Gang.Commands.SendPlayerRadioMessage(player, message, false);
            else NAPI.Chat.SendChatMessageToPlayer(player, "~r~ERROR: ~w~No tienes permiso.");
        }

        [Command("ayudatrabajo")]
        public void HelpJob(Client sender)
        {
            if (Data.Character[sender].Job == null)
            {
                sender.SendChatMessage("~r~ERROR: ~w~No tienes trabajo.");
                return;
            }

            if (Data.Character[sender].Job == (int)Job.JobType.Trucker) sender.SendChatMessage("~y~INFO: ~w~/trabajocamionero - /terminartrabajo");
            else if (Data.Character[sender].Job == (int)Job.JobType.Garbageman) sender.SendChatMessage("~y~INFO: ~w~/terminartrabajo");
            else if (Data.Character[sender].Job == (int)Job.JobType.ArmsDealer) sender.SendChatMessage("~y~INFO: ~w~/material - /vendermats - /terminartrabajo");
            else if (Data.Character[sender].Job == (int)Job.JobType.Courier)
            {
                sender.SendChatMessage("~y~INFO: ~w~Tu trabajo es obtener productos de las fábricas y venderlos a las repartidoras.");
                sender.SendChatMessage("~y~INFO: ~w~Puedes usar Burrito, Speedo, Rumpo y Yoga para cargar mas produtos.");
            }
            else if (Data.Character[sender].Job == (int)Job.JobType.ArmsDealer) sender.SendChatMessage("~y~INFO: ~w~Pulsa ~y~B ~w~para acceder al taxímetro.");
            else if (Data.Character[sender].Job == (int)Job.JobType.Guard) sender.SendChatMessage("~y~INFO: ~w~/proteger - /trabajoguardia - /terminartrabajo");
            sender.SendChatMessage("~y~INFO: ~w~/dejartrabajo");
        }

        [Command("ayudafaccion")]
        public void HelpFaction(Client sender)
        {
            if (sender.GetFactionID() == null) sender.SendChatMessage("~r~ERROR: ~w~No tienes faccion.");
            else
            {
                Faction.Faction faction = Faction.Main.Factions.FirstOrDefault(f => f.ID == sender.GetFactionID());

                if (faction == null)
                    sender.SendChatMessage("~r~ERROR: ~w~No tienes faccion.");
                else if (faction.Type == (int)Faction.FactionType.Police)
                {
                    sender.SendChatMessage("~y~INFO: ~w~(/m)egafono - (/r)adio - /f - /cuff - /uncuff");
                    sender.SendChatMessage("~y~INFO: ~w~/detener - /soltar - /detenidos - (/so)spechar");
                    sender.SendChatMessage("~y~INFO: ~w~/sospechosos - /limpiarregistro - /multar");
                }
                else if (faction.Type == (int)Faction.FactionType.Medic)
                {
                    sender.SendChatMessage("~y~INFO: ~w~/socorrer - /sanar - (/r)adio - /f - /uniforme");
                }
                else if (faction.Type == (int)Faction.FactionType.Army)
                {
                    sender.SendChatMessage("~y~INFO: ~w~(/m)egafono - (/r)adio - /f - /cuff - /uncuff");
                    sender.SendChatMessage("~y~INFO: ~w~/detenidos - (/so)spechar");
                    sender.SendChatMessage("~y~INFO: ~w~/sospechosos - /limpiarregistro - /multar");
                }

                if (Data.Character[sender].FacRank == 0)
                {
                    sender.SendChatMessage("~y~INFO: ~w~/invitar - /fdespedir - /promover - /degradar");
                    sender.SendChatMessage("~y~INFO: ~w~/cfvehiculo - /rfvehiculo - /fsvehiculopos");
                }
            }
        }

        [Command("dejartrabajo")]
        public void QuitJob(Client sender)
        {
            if (Data.Character[sender].Job == null) sender.SendChatMessage("~r~ERROR: ~w~No tienes trabajo.");
            else
            {
                sender.SendChatMessage("~g~ÉXITO: ~w~Has dejado tu trabajo.");
                Data.Character[sender].Job = null;
            }
        }

        [Command("do", "~y~USE: ~w~/do [accion]", GreedyArg = true)]
        public void Do(Client sender, string action)
        {
            sender.sendChatAction(action, 1);
        }

        [Command("me", "~y~USE: ~w~/me [accion]", GreedyArg = true)]
        public void Me(Client sender, string action)
        {
            sender.sendChatAction(action);
        }

        [Command("g", "~y~USE: ~w~/g(shout) [mensaje]", GreedyArg = true, Alias = "shout")]
        public void Shout(Client sender, string message)
        {
            var msg = $"{sender.Name} grita: {message.ToUpper()}!!";
            var players = sender.getPlayersInRadiusOfPlayer(30f);

            foreach (var player in players)
            {
                if (player == null) continue;
                else if (player.Dimension != sender.Dimension) continue;
                NAPI.Chat.SendChatMessageToPlayer(player, msg);
            }
        }

        [Command("s", "~y~USE: ~w~/s(low) [mensaje]", GreedyArg = true, Alias = "low")]
        public void Whisper(Client sender, string message)
        {
            var msg = $"{sender.Name} sussurra: {message.ToLower()}";
            var players = sender.getPlayersInRadiusOfPlayer(2f);

            foreach (var player in players)
            {
                if (player == null) continue;
                else if (player.Dimension != player.Dimension) continue;
                NAPI.Chat.SendChatMessageToPlayer(player, msg);
            }
        }

        [Command("pagar", "~y~USO: ~w~/pagar [jugador] [dinero]")]
        public void Pay(Client sender, string idOrName, int money)
        {
            var target = PlayeridManager.FindPlayer(idOrName);
            if (target == null) sender.SendChatMessage("~r~ERROR: ~w~Demasiados o ningún jugador encontrado.");
            else if (!target.IsLogged()) sender.SendChatMessage("~r~ERROR: ~w~El jugador no está conectado.");
            else if (sender == target) NAPI.Chat.SendChatMessageToPlayer(sender, "~r~ERROR: ~w~No puedes darte dinero a ti mismo.");
            else if (sender.Position.DistanceTo(target.Position) > 2f) NAPI.Chat.SendChatMessageToPlayer(sender, "~r~ERROR: ~w~El jugador está muy lejos.");
            else
            {
                if (sender.getMoney() < money)
                    NAPI.Chat.SendChatMessageToPlayer(sender, "~r~ERROR: ~w~No tienes suficiente dinero.");
                else if (money < 1)
                    NAPI.Chat.SendChatMessageToPlayer(sender, "~r~ERROR: ~w~No puedes usar números negativos.");
                else
                {
                    sender.giveMoney(-money);
                    target.giveMoney(money);
                    sender.sendChatAction($"le ha dado dinero a {target.Name}.");
                }
            }
        }

        [Command("b", "~r~USO: ~w~/b [messaje]", GreedyArg = true)]
        public void LocalChat(Client player, string message)
        {
            var players = player.getPlayersInRadiusOfPlayer(30f);
            foreach (Client c in players)
            {
                NAPI.Chat.SendChatMessageToPlayer(c, "~c~" + "[OOC] " + "~w~(( " + (player.Name) + ": " + message + " ))");
            }
        }

        [Command("cuenta")]
        public void Command_Stats(Client player)
        {
            // player data
            string[] name = player.Name.Split(' ');
            int money = Data.Character[player].Money;
            int bank = Data.Character[player].Bank;
            int level = Data.Character[player].Level;
            int experience = Data.Character[player].Experience;
            int nExperience = Data.Character[player].Level * 2;
            string job = Data.Character[player].Job == null ? "No" : Job.JobDefinitions.Jobs[(int)Data.Character[player].Job].Name;
            string faction = Data.Character[player].Faction == null ? "No" : Faction.Main.Factions.FirstOrDefault(f => f.ID == player.GetFactionID()).Name;
            string frank = Data.Character[player].Faction == null ? "No" : player.getRank();
            string phone = Data.Character[player].PhoneNumber == null ? "No" : Data.Character[player].PhoneNumber.ToString();
            int vehicleCount = Data.Character[player].Vehicles.Count;
            string license = Data.Character[player].DrivingLicense ? "Sí" : "No";
            int hoursPlayed = Convert.ToInt32(Math.Floor((Data.Character[player].PlayedTime / 60) / 60));
            string lastLogin = Data.Character[player].LastLogin.ToString("dd/MM/yyyy");
            string memberSince = Data.Character[player].CreatedAt.ToString("dd/MM/yyyy");

            // display data
            player.SendChatMessage("~g~________ CUENTA ________");
            player.SendChatMessage($"~c~Nombre: ~w~{name[0]} - ~c~Apellido: ~w~{name[1]} - ~c~Dinero: ~g~$~w~{money} - ~c~Banco: ~g~$~w~{bank}");
            player.SendChatMessage($"~c~Nivel: ~w~{level} - ~c~Experiência: ~w~{experience} / {nExperience} - Tiempo jugado: ~w~{hoursPlayed} horas");
            player.SendChatMessage($"~c~Trabajo: ~w~{job} - ~c~Facción: ~w~{faction} - ~c~Rango: ~w~{frank}");
            player.SendChatMessage($"~c~Teléfono: ~w~{phone} - ~c~Vehículos: ~w~{vehicleCount} - ~c~Licencia: ~w~{license}");
            player.SendChatMessage($"~c~Último acceso: ~w~{lastLogin} - ~c~Miembro desde: ~w~{memberSince}");
        }
    }
}
