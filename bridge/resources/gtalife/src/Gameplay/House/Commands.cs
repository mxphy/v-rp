﻿using System.IO;
using System.Linq;
using GTANetworkAPI;
using gtalife.src.Database.Models;

namespace gtalife.src.Gameplay.House
{
    class Commands : Script
    {
        [Command("crearcasa")]
        public void CMD_CreateHouse(Client player, int type, int price)
        {
            User user = NAPI.Data.GetEntityData(player, "User");
            if (user.Admin < 4)
            {
                player.SendChatMessage("~r~ERROR: ~w~No tienes permison.");
                return;
            }

            if (type < 0 || type >= HouseTypes.HouseTypeList.Count)
            {
                player.SendChatMessage("~r~ERROR: ~w~ID del tipo es inválido.");
                return;
            }

            House new_house = new House(Main.GetGuid(), string.Empty, type, player.Position, price, false);
            new_house.Dimension = Main.DimensionID;
            new_house.Save();

            Main.Houses.Add(new_house);
            Main.DimensionID++;
        }

        [Command("setcasanombre", GreedyArg = true)]
        public void CMD_HouseName(Client player, string new_name)
        {
            User user = NAPI.Data.GetEntityData(player, "User");
            if (user.Admin < 4)
            {
                player.SendChatMessage("~r~ERROR: ~w~No tienes permison.");
                return;
            }

            if (!player.HasData("HouseMarker_ID"))
            {
                player.SendChatMessage("~r~ERROR: ~w~Párese en el marcador de entrada de la casa que desea editar.");
                return;
            }

            House house = Main.Houses.FirstOrDefault(h => h.ID == player.GetData("HouseMarker_ID"));
            if (house == null) return;

            house.SetName(new_name);
            player.SendChatMessage(string.Format("~b~CASA: ~w~Nombre ajustado a ~y~\"{0}\".", new_name));
        }

        [Command("setcasatipo")]
        public void CMD_HouseType(Client player, int new_type)
        {
            User user = NAPI.Data.GetEntityData(player, "User");
            if (user.Admin < 4)
            {
                player.SendChatMessage("~r~ERROR: ~w~No tienes permison.");
                return;
            }

            if (!player.HasData("HouseMarker_ID"))
            {
                player.SendChatMessage("~r~ERROR: ~w~Párese en el marcador de entrada de la casa que desea editar.");
                return;
            }

            if (new_type < 0 || new_type >= HouseTypes.HouseTypeList.Count)
            {
                player.SendChatMessage("~r~ERROR: ~w~ID del tipo es inválido.");
                return;
            }

            House house = Main.Houses.FirstOrDefault(h => h.ID == player.GetData("HouseMarker_ID"));
            if (house == null) return;

            house.SetType(new_type);
            player.SendChatMessage(string.Format("~b~CASA: ~w~Tipo ajustado para ~y~{0}.", HouseTypes.HouseTypeList[new_type].Name));
        }

        [Command("setcasaprecio")]
        public void CMD_HousePrice(Client player, int new_price)
        {
            User user = NAPI.Data.GetEntityData(player, "User");
            if (user.Admin < 4)
            {
                player.SendChatMessage("~r~ERROR: ~w~No tienes permison.");
                return;
            }

            if (!player.HasData("HouseMarker_ID"))
            {
                player.SendChatMessage("~r~ERROR: ~w~Párese en el marcador de entrada de la casa que desea editar.");
                return;
            }

            House house = Main.Houses.FirstOrDefault(h => h.ID == player.GetData("HouseMarker_ID"));
            if (house == null) return;

            house.SetPrice(new_price);
            player.SendChatMessage(string.Format("~b~CASA: ~w~Precio ajustado para ~g~${0:n0}.", new_price));
        }

        [Command("borrarcasa")]
        public void CMD_RemoveHouse(Client player)
        {
            User user = NAPI.Data.GetEntityData(player, "User");
            if (user.Admin < 4)
            {
                player.SendChatMessage("~r~ERROR: ~w~No tienes permison.");
                return;
            }

            if (!player.HasData("HouseMarker_ID"))
            {
                player.SendChatMessage("~r~ERROR: ~w~Párese en el marcador de entrada de la casa que desea borrar.");
                return;
            }

            House house = Main.Houses.FirstOrDefault(h => h.ID == player.GetData("HouseMarker_ID"));
            if (house == null) return;

            house.Destroy();
            Main.Houses.Remove(house);

            string house_file = Main.HOUSE_SAVE_DIR + Path.DirectorySeparatorChar + house.ID + ".json";
            if (File.Exists(house_file)) File.Delete(house_file);
        }

        [Command("casacmds")]
        public void AdminCommands(Client player)
        {
            NAPI.Chat.SendChatMessageToPlayer(player, "!{#a5f413}~~~~~~~~~~~ Comandos Admin ~~~~~~~~~~~");
            NAPI.Chat.SendChatMessageToPlayer(player, "* /crearcasa - /setcasanombre - /setcasatipo - /setcasaprecio - /borrarcasa");
            NAPI.Chat.SendChatMessageToPlayer(player, "!{#a5f413}~~~~~~~~~~~ Comandos Admin ~~~~~~~~~~~");
        }
    }
}
