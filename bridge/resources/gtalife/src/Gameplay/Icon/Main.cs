﻿using GTANetworkAPI;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace gtalife.src.Gameplay.Icon
{
    public class Main : Script
    {
        // settings
        public static string BLIP_SAVE_DIR = "data/Blip";
        public static int SAVE_INTERVAL = 120;

        public static List<Icon> Icons = new List<Icon>();

        #region Methods
        public static Guid GetGuid()
        {
            Guid new_guid;

            do
            {
                new_guid = Guid.NewGuid();
            } while (Icons.Count(h => h.ID == new_guid) > 0);

            return new_guid;
        }
        #endregion

        #region Events
        [ServerEvent(Event.ResourceStart)]
        public void OnResourceStart()
        {
            // load settings
            if (NAPI.Resource.HasSetting(this, "blipDirName")) BLIP_SAVE_DIR = NAPI.Resource.GetSetting<string>(this, "blipDirName");

            BLIP_SAVE_DIR = NAPI.Resource.GetResourceFolder(this) + Path.DirectorySeparatorChar + BLIP_SAVE_DIR;
            if (!Directory.Exists(BLIP_SAVE_DIR)) Directory.CreateDirectory(BLIP_SAVE_DIR);

            if (NAPI.Resource.HasSetting(this, "saveInterval")) SAVE_INTERVAL = NAPI.Resource.GetSetting<int>(this, "saveInterval");

            // load icons
            foreach (string file in Directory.EnumerateFiles(BLIP_SAVE_DIR, "*.json"))
            {
                Icon icon = JsonConvert.DeserializeObject<Icon>(File.ReadAllText(file));
                Icons.Add(icon);
            }

            NAPI.Util.ConsoleOutput("Loaded {0} blips.", Icons.Count);
        }

        [ServerEvent(Event.ResourceStop)]
        public void OnResourceStop()
        {
            foreach (Icon icon in Icons)
            {
                icon.Save(true);
                icon.Destroy(true);
            }

            Icons.Clear();
        }
        #endregion
    }
}
