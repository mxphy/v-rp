﻿using System;
using System.Linq;
using System.Collections.Generic;
using GTANetworkAPI;
using System.IO;
using Newtonsoft.Json;
using gtalife.src.Player.Utils;

namespace gtalife.src.Faction
{
    public class Main : Script
    {
        // settings
        public static string FACTION_SAVE_DIR = "data/Faction";
        public static int SAVE_INTERVAL = 120;

        public static List<Faction> Factions = new List<Faction>();

        #region Methods
        public static Guid GetGuid()
        {
            Guid new_guid;

            do
            {
                new_guid = Guid.NewGuid();
            } while (Factions.Count(h => h.ID == new_guid) > 0);

            return new_guid;
        }

        public static void SendFactionMessage(Guid id, string message)
        {
            foreach (var p in NAPI.Pools.GetAllPlayers())
            {
                if (p.GetFactionID() == id) p.SendChatMessage(message);
            }
        }

        public static void SendFactionTypeMessage(FactionType type, string message)
        {
            foreach (var p in NAPI.Pools.GetAllPlayers())
            {
                Faction faction = Factions.FirstOrDefault(f => f.ID == p.GetFactionID());
                if (faction != null && faction.Type == (int)type) p.SendChatMessage(message);
            }
        }

        public static void SendPlayerFactionMessage(Client player, string message, bool ic = true)
        {
            if (player.GetFactionID() == null) return;

            Faction faction = Factions.FirstOrDefault(f => f.ID == player.GetFactionID());
            if (faction == null)
            {
                player.SetFactionID(null);
                player.SetFactionRank(null);
                player.SendChatMessage("~r~ERROR: ~w~Tu facción fue eliminada.");
                return;
            }

            if (ic)
            {
                var players = NAPI.Pools.GetAllPlayers();
                foreach (var p in players)
                {
                    if (p.GetFactionID() == faction.ID) p.SendChatMessage($"!{{#3399FF}}[Radio] {faction.Ranks[(int)player.GetFactionRank()]} {player.Name} diz: {message}");
                    else
                    {
                        if (p.Dimension != player.Dimension || p.Position.DistanceTo(player.Position) > 30f) continue;
                        p.SendChatMessage($"!{{#cccccc}}(Radio) {player.Name} diz: {message}");
                    }
                }
            }
            else
            {
                var players = NAPI.Pools.GetAllPlayers();
                foreach (var p in players)
                {
                    if (p.GetFactionID() != faction.ID) continue;
                    p.SendChatMessage($"!{{#2e85dd}}[OOC] (( {faction.Ranks[(int)player.GetFactionRank()]} {player.Name}: {message} ))");
                }
            }
        }
        #endregion

        #region Events
        [ServerEvent(Event.ResourceStart)]
        public void OnResourceStart()
        {
            // load settings
            if (NAPI.Resource.HasSetting(this, "factionDirName")) FACTION_SAVE_DIR = NAPI.Resource.GetSetting<string>(this, "factionDirName");

            FACTION_SAVE_DIR = NAPI.Resource.GetResourceFolder(this) + Path.DirectorySeparatorChar + FACTION_SAVE_DIR;
            if (!Directory.Exists(FACTION_SAVE_DIR)) Directory.CreateDirectory(FACTION_SAVE_DIR);

            if (NAPI.Resource.HasSetting(this, "saveInterval")) SAVE_INTERVAL = NAPI.Resource.GetSetting<int>(this, "saveInterval");

            // load factions
            foreach (string file in Directory.EnumerateFiles(FACTION_SAVE_DIR, "*.json"))
            {
                Faction faction = JsonConvert.DeserializeObject<Faction>(File.ReadAllText(file));
                Factions.Add(faction);

                // vehicles
                foreach (FactionVehicle vehicle in faction.Vehicles) vehicle.Create();
            }

            NAPI.Util.ConsoleOutput("Loaded {0} factions.", Factions.Count);
        }

        [ServerEvent(Event.ResourceStop)]
        public void OnResourceStop()
        {
            foreach (Faction house in Factions)
            {
                house.Save(true);
            }

            Factions.Clear();
        }
        #endregion
    }
}
