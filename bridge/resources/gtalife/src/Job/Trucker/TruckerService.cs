﻿using GTANetworkAPI;
using gtalife.src.Flags;
using gtalife.src.Managers;
using gtalife.src.Player.Inventory.Classes;
using gtalife.src.Player.Inventory.Extensions;
using gtalife.src.Player.Utils;
using System;

namespace gtalife.src.Job.Trucker
{
    class TruckerService : Script
    {
        const VehicleHash TRUCKER_VEHICLE = (VehicleHash)2112052861;
        readonly float[,] TRUCK_SPAWN_POSITIONS = new float[,]
        {
            { -439.3017f, 6140.786f, 31.71225f, -0.01179021f, 0.03215728f, -133.9911f },
            { -443.3017f, 6140.786f, 31.71225f, -0.01179021f, 0.03215728f, -133.9911f },
        };

        public static void FinishService(Client player)
        {
            if (player.HasData("TRUCKER_JOB_BOX"))
            {
                if (NAPI.Entity.DoesEntityExist(player.GetData("TRUCKER_JOB_BOX")))
                {
                    NAPI.Entity.DeleteEntity(player.GetData("TRUCKER_JOB_BOX"));
                }
            }

            if (Main.PlayerJobVehicle.ContainsKey(player))
            {
                if (NAPI.Entity.DoesEntityExist(Main.PlayerJobVehicle[player].NetHandle)) NAPI.Entity.DeleteEntity(Main.PlayerJobVehicle[player].NetHandle);
                Main.PlayerJobVehicle.Remove(player);
            }

            player.ResetData("TRUCK_JOB_OPTION");
            player.TriggerEvent("ResetTruckerJob");
        }

        [ServerEvent(Event.PlayerDisconnected)]
        public void OnPlayerDisconnected(Client player, DisconnectionType type, string reason)
        {
            if (player.HasData("TRUCKER_JOB_BOX"))
            {
                if (NAPI.Entity.DoesEntityExist(player.GetData("TRUCKER_JOB_BOX")))
                {
                    NAPI.Entity.DeleteEntity(player.GetData("TRUCKER_JOB_BOX"));
                }
            }
        }

        [ServerEvent(Event.PlayerEnterVehicle)]
        public void OnPlayerEnterVehicle(Client player, Vehicle vehicle, sbyte seatID)
        {
            if (Main.PlayerJobVehicle.ContainsKey(player))
            {
                if (vehicle == Main.PlayerJobVehicle[player].NetHandle && player.GetJob() == (int)JobType.Trucker)
                {
                    player.TriggerEvent("OnPlayerEnterOnTruckJobVehicle");
                }
            }
        }

        [RemoteEvent("TruckerSelectOption")]
        public void TruckerSelectOption(Client sender, object[] arguments)
        {
            int option = (int)arguments[0];

            if (Main.PlayerJobVehicle.ContainsKey(sender))
            {
                if (NAPI.Entity.DoesEntityExist(Main.PlayerJobVehicle[sender].NetHandle)) NAPI.Entity.DeleteEntity(Main.PlayerJobVehicle[sender].NetHandle);
                Main.PlayerJobVehicle.Remove(sender);
            }

            if (sender.HasData("TRUCKER_JOB_BOX"))
                if (NAPI.Entity.DoesEntityExist(sender.GetData("TRUCKER_JOB_BOX")))
                    NAPI.Entity.DeleteEntity(sender.GetData("TRUCKER_JOB_BOX"));

            switch (option)
            {
                case 0:
                    sender.SendChatMessage("~y~INFO: ~w~Seleccionaste: ~y~Medicina~w~.");
                    break;
                case 1:
                    sender.SendChatMessage("~y~INFO: ~w~Seleccionaste: ~y~Ropa~w~.");
                    break;
                case 2:
                    sender.SendChatMessage("~y~INFO: ~w~Seleccionaste: ~y~YouTool~w~.");
                    break;
                case 3:
                    sender.SendChatMessage("~y~INFO: ~w~Seleccionaste: ~y~Contrabando~w~.");
                    break;
            }

            sender.SetData("TRUCK_JOB_OPTION", option);
            sender.SendChatMessage("~y~INFO: ~w~Carga el camión y ve a entregarlo.");

            Random random = new Random();
            int i = random.Next(0, TRUCK_SPAWN_POSITIONS.GetLength(0));

            var truck = VehicleManager.CreateVehicle(TRUCKER_VEHICLE, new Vector3(TRUCK_SPAWN_POSITIONS[i, 0], TRUCK_SPAWN_POSITIONS[i, 1], TRUCK_SPAWN_POSITIONS[i, 2]), new Vector3(TRUCK_SPAWN_POSITIONS[i, 3], TRUCK_SPAWN_POSITIONS[i, 4], TRUCK_SPAWN_POSITIONS[i, 5]), 0, 1);
            truck.EngineStatus = false;

            Main.PlayerJobVehicle.Add(sender, new JobVehicle { NetHandle = truck.Handle, DeleteAt = DateTime.Now.AddMinutes(3) });

            sender.TriggerEvent("StartTruckerJob", truck.Position.X, truck.Position.Y, truck.Position.Z);
        }

        [RemoteEvent("OnFinishTruckerJob")]
        public void OnFinishTruckerJob(Client sender, object[] arguments)
        {
            int money = 0;

            switch (sender.GetData("TRUCK_JOB_OPTION"))
            {
                case 0:
                    money += 400;
                    sender.giveItem(ItemID.FirstAidKit_Tier1, 1);
                    break;
                case 1:
                    money += 460;
                    break;
                case 2:
                    money += 290;
                    break;
                case 3:
                    money += 850;
                    sender.giveItem(ItemID.Material, 1);
                    break;
            }

            sender.SendChatMessage($"~g~ÉXITO: ~w~Terminaste el trabajo, has ganado: ~g~${money}");
            sender.giveMoney(money);

            if (Main.PlayerJobVehicle.ContainsKey(sender))
            {
                if (NAPI.Entity.DoesEntityExist(Main.PlayerJobVehicle[sender].NetHandle)) NAPI.Entity.DeleteEntity(Main.PlayerJobVehicle[sender].NetHandle);
                Main.PlayerJobVehicle.Remove(sender);
            }

            sender.ResetData("TRUCK_JOB_OPTION");
        }

        [RemoteEvent("TruckerCarryBoxAnimPlay")]
        public void TruckerCarryBoxAnimPlay(Client sender, object[] arguments)
        {
            var box = NAPI.Object.CreateObject(1302435108, sender.Position, new Vector3(0, 0, 0));
            NAPI.Entity.AttachEntityToEntity(box, sender, "SKEL_L_Hand", new Vector3(0.20, 0.1, 0.25), new Vector3(0, 90, 90));
            sender.SetData("TRUCKER_JOB_BOX", box);
            sender.PlayAnimation("anim@heists@box_carry@", "idle", (int)(AnimationFlags.Loop | AnimationFlags.OnlyAnimateUpperBody | AnimationFlags.AllowPlayerControl));
        }

        [RemoteEvent("TruckerCarryBoxAnimStop")]
        public void TruckerCarryBoxAnimStop(Client sender, object[] arguments)
        {
            var _box = sender.GetData("TRUCKER_JOB_BOX");
            NAPI.Entity.DeleteEntity(_box);
            sender.ResetData("TRUCKER_JOB_BOX");
            sender.StopAnimation();
        }

        [Command("trabajocamionero")]
        public void CMD_TruckerService(Client sender)
        {
            if (sender.GetJob() == (int)JobType.Trucker) sender.TriggerEvent("ShowTruckerMenu");                
            else sender.SendChatMessage("~r~ERROR: ~w~No eres camionero.");
        }
    }
}
