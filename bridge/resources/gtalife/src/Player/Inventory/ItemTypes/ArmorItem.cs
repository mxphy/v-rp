﻿using GTANetworkAPI;
using gtalife.src.Player.Inventory.Classes;
using gtalife.src.Player.Inventory.Interfaces;

namespace gtalife.src.Player.Inventory.ItemTypes
{
    public class ArmorItem : BaseItem, IDroppable
    {
        public int Value { get; set; }
        public WorldModel DropModel { get; set; }

        public ArmorItem(string name, string description, int stackSize, int armorValue, WorldModel dropModel) : base(name, description, stackSize)
        {
            Value = armorValue;
            DropModel = dropModel;
        }

        public override bool Use(Client player)
        {
            if (player.Armor == 100)
            {
                player.SendChatMessage("~r~ERROR: ~w~Tienes armadura completa.");
                return false;
            }

            player.SendChatMessage($"~y~INFO: ~w~Armadura usada. (+{Value})");
            player.Armor = ((player.Armor + Value > 100) ? 100 : player.Armor + Value);
            return true;
        }
    }
}
