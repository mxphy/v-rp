﻿using System;
using GTANetworkAPI;
using gtalife.src.Player.Utils;
using System.Timers;

namespace gtalife.src.Gameplay
{
    class PayDay : Script
    {
        DateTime lastPaydayAt = DateTime.Now;

        [ServerEvent(Event.ResourceStart)]
        public void OnResourceStart()
        {
            Timer timer = new Timer();
            timer.Elapsed += new ElapsedEventHandler(OnUpdate);
            timer.Interval = 5000;
            timer.Enabled = true;
        }

        private void OnUpdate(object sender, ElapsedEventArgs e)
        {
            if ((DateTime.Now - lastPaydayAt).TotalMinutes > 60)
            {
                foreach (var player in NAPI.Pools.GetAllPlayers())
                {
                    if (!player.IsLogged()) continue;

                    // Payment
                    float interestRate = 0.001f;
                    int payment = Player.Data.Character[player].Level * 150;
                    int interestTotal = (int)Math.Round(Player.Data.Character[player].Bank * interestRate);
                    Player.Data.Character[player].Bank += (interestTotal + payment);

                    // Experience
                    bool hasLeveledUp = false;
                    int neededExp = Player.Data.Character[player].Level * 2;

                    Player.Data.Character[player].Experience++;
                    if (Player.Data.Character[player].Experience >= neededExp)
                    {
                        Player.Data.Character[player].Level++;
                        Player.Data.Character[player].Experience = 0;
                        hasLeveledUp = true;
                        neededExp = Player.Data.Character[player].Level * 2;
                    }

                    player.SendChatMessage("/________________ RESUMEN DE TU CUENTA ________________/");
                    player.SendChatMessage($"Paga diário: ~g~+${payment} ~w~| Interesses: ~g~+${interestTotal}");
                    player.SendChatMessage($"Saldo bancário: ~g~${Player.Data.Character[player].Bank} ~w~| Total: ~g~+${payment + interestTotal}");
                    player.SendChatMessage($"Nível: ~g~{Player.Data.Character[player].Level} ~w~| Experiencia: ~g~{Player.Data.Character[player].Experience}/{neededExp}");
                    if (hasLeveledUp) player.SendChatMessage($"~g~¡Felicitaciones! ¡Subió de nivel!");
                    player.SendChatMessage("/________________ FIN DEL RESUMEN ______________________/");

                    lastPaydayAt = DateTime.Now;
                }
            }
        }
    }
}
