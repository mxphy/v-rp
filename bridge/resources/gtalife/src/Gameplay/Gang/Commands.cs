﻿using GTANetworkAPI;
using gtalife.src.Admin;
using gtalife.src.Database.Models;
using gtalife.src.Managers;
using gtalife.src.Player.Utils;
using System.IO;
using System.Linq;

namespace gtalife.src.Gameplay.Gang
{
    class Commands : Script
    {
        // Player
        [Command("creargang", "~y~USO: ~w~/creargang [nombre]", GreedyArg = true)]
        public void CMD_CreateGang(Client player, string name)
        {
            int count = Main.Gangs.Count(x => x.ID == Player.Data.Character[player].Gang);

            if (count > 0) player.SendChatMessage("~r~ERROR: ~w~Ya estás en una gang.");
            else if (player.getMoney() < 150000) player.SendChatMessage("~r~ERROR: ~w~No tienes suficiente dinero.");
            else if (Main.Gangs.Count(x => x.Name == name) > 0) player.SendChatMessage("~r~ERROR: ~w~Este nombre ya esta en uso.");
            else
            {
                player.giveMoney(-150000);

                player.SendChatMessage($"~g~ÉXITO: ~w~Usted creó la gang: {name}. (( /ayudagang ))");

                Gang new_gang = new Gang(Main.GetGuid(), Player.Data.Character[player].Id, name, GangType.Gang);
                new_gang.Save();

                Player.Data.Character[player].Gang = new_gang.ID;
                Player.Data.Character[player].GangRank = 0;

                Main.Gangs.Add(new_gang);
            }
        }

        [Command("editargangrank", "~y~USO: ~w~/editargangrank [0-5] [nombre]", GreedyArg = true)]
        public void CMD_EditGangRank(Client player, int rank, string name)
        {
            if (Player.Data.Character[player].Gang != null) 
            {
                Gang gang = Main.Gangs.FirstOrDefault(x => x.ID == Player.Data.Character[player].Gang);

                if (gang == null) player.SendChatMessage("~r~ERROR: ~w~Usted no es dueño de una gang.");
                else if (gang.CharacterId != Player.Data.Character[player].Id) player.SendChatMessage("~r~ERROR: ~w~Usted no es dueño de la gang.");
                else if (rank < 0 || rank > 5) player.SendChatMessage("~r~ERROR: ~w~Rank inválido.");
                else
                {
                    player.SendChatMessage($"~y~INFO: ~w~Usted cambió el rank {rank} para {name}.");

                    gang.Ranks[rank] = name;
                    gang.Save(true);
                }
            }
            else player.SendChatMessage("~r~ERROR: ~w~Usted no es dueño de una gang.");
        }

        [Command("editarrank", "~y~USO: ~w~/editarrank [jugador] [1-5]")]
        public void CMD_EditRank(Client player, string idOrName, int rank)
        {
            if (!player.IsGangMember())
            {
                player.SendChatMessage("~r~ERROR: ~w~Usted no es dueño de una gang.");
                return;
            }

            Gang gang = Main.Gangs.FirstOrDefault(x => x.ID == player.GetGangId());

            var target = PlayeridManager.FindPlayer(idOrName);
            if (target == null) player.SendChatMessage("~r~ERROR: ~w~Demasiados o ningún jugador encontrado.");
            else if (!target.IsLogged()) player.SendChatMessage("~r~ERROR: ~w~El jugador no está conectado.");
            else if (gang == null) player.SendChatMessage("~r~ERROR: ~w~Usted no es dueño de una gang.");
            else if (player.GetGangRank() > 0) player.SendChatMessage("~r~ERROR: ~w~Usted no es dueño de la gang.");
            else if (rank < 1 || rank > 5) player.SendChatMessage("~r~ERROR: ~w~Rank inválido.");
            else if (target == player) player.SendChatMessage("~r~ERROR: ~w~No puedes hacer isto.");
            else if (target.GetGangId() != player.GetGangId()) player.SendChatMessage("~r~ERROR: ~w~El jugador no es de su gang.");

            if (player != target) target.SendChatMessage($"~y~INFO: ~w~{player.Name} cambió su rank para {gang.Ranks[rank]}.");
            player.SendChatMessage($"~y~INFO: ~w~Usted cambió el rank de {target.Name} para {gang.Ranks[rank]}.");

            target.SetGangRank(rank);
        }

        [Command("borrargang")]
        public void CMD_DeleteGang(Client player)
        {
            if (Player.Data.Character[player].Gang == null)
            {
                player.SendChatMessage("~r~ERROR: ~w~Usted no es dueño de una gang.");
                return;
            }

            Gang gang = Main.Gangs.FirstOrDefault(x => x.ID == Player.Data.Character[player].Gang);
            if (gang == null)
            {
                player.SendChatMessage("~r~ERROR: ~w~Tu gang fue borrada.");
                Player.Data.Character[player].Gang = null;
                return;
            }
            else if (gang.CharacterId != Player.Data.Character[player].Id)
            {
                player.SendChatMessage("~r~ERROR: ~w~Usted no es dueño de la gang.");
                return;
            }

            Main.SendGangMessage(gang, $"~y~INFO: ~w~{player.Name} ha borrado la gang!");

            foreach (var member in NAPI.Pools.GetAllPlayers())
            {
                if (!member.IsLogged()) continue;                
                if (Player.Data.Character[player].Gang != gang.ID) continue;

                Player.Data.Character[player].Gang = null;
            }

            Main.Gangs.Remove(gang);

            string gang_file = Main.GANG_SAVE_DIR + Path.DirectorySeparatorChar + gang.ID + ".json";
            if (File.Exists(gang_file)) File.Delete(gang_file);

            // Remove offline players
            using (var ctx = new DefaultDbContext())
            {
                var characters = ctx.Characters.Where(x => x.Gang == gang.ID);
                foreach (var c in characters)
                {
                    c.Gang = null;
                }
                ctx.SaveChangesAsync();
            }
        }

        [Command("reclutar", "~y~USO: ~w~/reclutar [jugador]", GreedyArg = true)]
        public void CMD_InviteGang(Client player, string idOrName)
        {
            if (!player.IsGangMember())
            {
                player.SendChatMessage("~r~ERROR: ~w~Usted no es dueño de una gang.");
                return;
            }

            Gang gang = Main.Gangs.FirstOrDefault(x => x.ID == player.GetGangId());
            if (gang == null)
            {
                player.SetGangId(null);
                player.SendChatMessage("~r~ERROR: ~w~Usted no es dueño de una gang.");
                return;
            }
            else if (player.GetGangRank() > 0)
            {
                player.SendChatMessage("~r~ERROR: ~w~Usted no es dueño de una gang.");
                return;
            }

            var target = PlayeridManager.FindPlayer(idOrName);
            if (target == null) player.SendChatMessage("~r~ERROR: ~w~Demasiados o ningún jugador encontrado.");
            else if (!target.IsLogged()) player.SendChatMessage("~r~ERROR: ~w~El jugador no está conectado.");
            else if (target.GetGangId() != null) player.SendChatMessage("~r~ERROR: ~w~El jugador ya es de una gang.");
            else if (target == player) player.SendChatMessage("~r~ERROR: ~w~No puedes hacer isto.");
            else
            {
                target.SendChatMessage($"~y~INFO: ~w~Quieres unirte a ~b~{gang.Name}~w~ (( /acceptargang ))");
                player.SendChatMessage($"~y~INFO: ~w~Invitaste a {target.Name} a tu gang.");

                target.SetData("GANG_INVITE", gang);
            }
        }

        [Command("acceptargang")]
        public void CMD_AcceptGang(Client player)
        {
            if (!player.HasData("GANG_INVITE"))
            {
                player.SendChatMessage("~r~ERROR: ~w~Ninguna gang te invitó");
                return;
            }
            
            Gang gang = player.GetData("GANG_INVITE");

            Player.Data.Character[player].Gang = gang.ID;
            Player.Data.Character[player].GangRank = gang.Ranks.Count - 1;

            Main.SendGangMessage(gang, $"~y~INFO: ~w~¡{player.Name} se unió a la gang!");

            player.ResetData("GANG_INVITE");
        }

        [Command("gdespedir", "~y~USO: ~w~/gdespedir [jugador]", GreedyArg = true)]
        public void CMD_UnvinteGang(Client player, string idOrName)
        {
            if (!player.IsGangMember())
            {
                player.SendChatMessage("~r~ERROR: ~w~Usted no es dueño de una gang.");
                return;
            }

            Gang gang = Main.Gangs.FirstOrDefault(x => x.ID == player.GetGangId());
            if (gang == null)
            {
                player.SetGangId(null);
                player.SendChatMessage("~r~ERROR: ~w~Usted no es dueño de una gang.");
                return;
            }
            else if (player.GetGangRank() > 0)
            {
                player.SendChatMessage("~r~ERROR: ~w~Usted no es dueño de una gang.");
                return;
            }

            var target = PlayeridManager.FindPlayer(idOrName);
            if (target == null) player.SendChatMessage("~r~ERROR: ~w~Demasiados o ningún jugador encontrado.");
            else if (!target.IsLogged()) player.SendChatMessage("~r~ERROR: ~w~El jugador no está conectado.");
            else if (target.GetGangId() != player.GetGangId()) player.SendChatMessage("~r~ERROR: ~w~El jugador no es de su gang.");
            else
            {
                Main.SendGangMessage(gang, $"~y~INFO: ~w~¡{player.Name} fue expulsado de la gang!");
                target.SetGangId(null);
            }
        }

        [Command("dejargang")]
        public void CMD_LeaveGang(Client player)
        {            
            if (Player.Data.Character[player].Gang == null)
            {
                player.SendChatMessage("~r~ERROR: ~w~Usted no es miembro de una gang.");
                return;
            }

            Gang gang = Main.Gangs.FirstOrDefault(x => x.ID == Player.Data.Character[player].Gang);
            if (gang == null)
            {
                Player.Data.Character[player].Gang = null;
                player.SendChatMessage("~r~ERROR: ~w~Usted no es miembro de una gang.");
                return;
            }
            else if (gang.CharacterId == Player.Data.Character[player].Id)
            {
                player.SendChatMessage("~r~ERROR: ~w~Usted es el dueño de la gang.");
                return;
            }

            Main.SendGangMessage(gang, $"~y~INFO: ~w~¡{player.Name} dejó la gang!");
            Player.Data.Character[player].Gang = null;
        }

        public static void SendPlayerRadioMessage(Client player, string message, bool ic = true)
        {            
            if (Player.Data.Character[player].Gang == null)
            {
                player.SendChatMessage("~r~ERROR: ~w~Usted no es miembro de una gang.");
                return;
            }

            Gang gang = Main.Gangs.FirstOrDefault(x => x.ID == Player.Data.Character[player].Gang);
            if (gang == null)
            {
                Player.Data.Character[player].Gang = null;
                player.SendChatMessage("~r~ERROR: ~w~Usted no es miembro de una gang.");
                return;
            }

            if (ic) Main.SendGangMessage(gang, $"~o~[GANG] ~w~{player.Name} {gang.Ranks[Player.Data.Character[player].GangRank]}: {message}");
            else Main.SendGangMessage(gang, $"~o~[OOC-GANG] ~w~{player.Name} {gang.Ranks[Player.Data.Character[player].GangRank]}: {message}");
        }

        // Admin
        [Command("zonacmds")]
        public void CMD_ZoneCommands(Client player)
        {
            if (!player.IsAdmin()) player.SendChatMessage("~r~ERROR: ~w~No tienes permison.");
            else
            {
                NAPI.Chat.SendChatMessageToPlayer(player, "!{#a5f413}~~~~~~~~~~~ Comandos Zona ~~~~~~~~~~~");
                NAPI.Chat.SendChatMessageToPlayer(player, "* /crearzona - /borrarzona");
                NAPI.Chat.SendChatMessageToPlayer(player, "!{#a5f413}~~~~~~~~~~~ Comandos Zona ~~~~~~~~~~~");
            }
        }

        [Command("crearzona")]
        public void CMD_CreateZone(Client player)
        {
            if (!player.IsAdmin()) player.SendChatMessage("~r~ERROR: ~w~No tienes permison.");
            else
            {
                GangZone new_zone = new GangZone(Main.GetGuid(), null, player.Position);
                new_zone.Save();
                Main.GangZones.Add(new_zone);

                player.SendChatMessage($"~g~ÉXITO: ~w~Creaste una gang zone.");
            }
        }

        [Command("borrarzona")]
        public void CMD_RemoveZone(Client player)
        {
            if (!player.IsAdmin()) player.SendChatMessage("~r~ERROR: ~w~No tienes permison.");
            else if (!player.HasData("GangZoneMarker_ID")) player.SendChatMessage("~r~ERROR: ~w~Párese en el marcador de la zona que desea borrar.");
            else
            {
                GangZone zone = Main.GangZones.FirstOrDefault(z => z.ID == player.GetData("GangZoneMarker_ID"));
                if (zone == null) return;

                zone.Destroy();
                Main.GangZones.Remove(zone);

                player.SendChatMessage($"~g~ÉXITO: ~w~Borraste la gang zone.");
                player.ResetData("GangZoneMarker_ID");
                player.TriggerEvent("ShowGangZoneText", 0);

                string zone_file = Main.GANG_ZONE_SAVE_DIR + Path.DirectorySeparatorChar + zone.ID + ".json";
                if (File.Exists(zone_file)) File.Delete(zone_file);
            }
        }
    }
}
