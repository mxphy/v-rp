let browser = null;
var characters = null;

mp.events.add('UpdateCharactersList', (args) => {
    characters = args;

    if (characters != null) {
        if (JSON.parse(characters).length > 0) {
            mp.events.call('ApplyCharacterFeatures', 0);
        }
    }
    
    browser.execute(`updateList(${characters});`);
});

mp.events.add('ApplyCharacterFeatures', (i) => {
    var character = JSON.parse(characters);
    
    mp.events.callRemote("SetPlayerSkin", character[i].Gender ? 'FreemodeFemale01' : 'FreeModeMale01');
    mp.events.callRemote("SetPlayerClothes", 2, character[i].Trait.HairType, 0);

    mp.players.local.setHeadBlendData(character[i].Trait.FaceFirst, character[i].Trait.FaceSecond, 0, character[i].Trait.SkinFirst, character[i].Trait.SkinSecond, 0, character[i].Trait.FaceMix, character[i].Trait.SkinMix, 0, false);
    mp.players.local.setHairColor(character[i].Trait.HairColor, character[i].Trait.HairHighlight);
    mp.players.local.setEyeColor(character[i].Trait.EyeColor);
    mp.players.local.setHeadOverlayColor(2, 1, character[i].Trait.EyebrowsColor1, character[i].Trait.EyebrowsColor2);

    if (character[i].Trait.Beard != null) mp.players.local.setHeadOverlayColor(1, 1, character[i].Trait.BeardColor, character[i].Trait.BeardColor);
    if (character[i].Trait.Makeup != null) mp.players.local.setHeadOverlayColor(4, 0, character[i].Trait.MakeupColor, character[i].Trait.MakeupColor);
    if (character[i].Trait.Lipstick != null) mp.players.local.setHeadOverlayColor(8, 2, character[i].Trait.LipstickColor, character[i].Trait.LipstickColor);

    for (var j = 0; j < character[i].Clothes.length; j++) {
        if (character[i].Clothes[j].IsProp) mp.events.callRemote("SetPlayerAccessory", character[i].Clothes[j].Slot, character[i].Clothes[j].Drawable, character[i].Clothes[j].Texture);
        else mp.events.callRemote("SetPlayerClothes", character[i].Clothes[j].Slot, character[i].Clothes[j].Drawable, character[i].Clothes[j].Texture);
    }
});

mp.events.add('DeleteCharacter', (id) => {
    var character = JSON.parse(characters);
    mp.events.callRemote("DeleteCharacter", character[id].Id);
});

mp.events.add('ShowCharacterSelector', () => {
    browser = mp.browsers.new('package://gtalife/assets/views/character_selector.html');

    mp.gui.chat.activate(false);
    mp.gui.chat.show(false);
    mp.gui.cursor.visible = true;
    
    // Move scene position
    mp.events.callRemote("SetPlayerPos", 402.9198, -996.5348, -100.00024, true);
    mp.events.callRemote("SetPlayerRot", 176.8912);
    
    var start_camera = mp.cameras.new("start", {x: 400.9627, y: -1005.109, z: -99.00404}, {x:0, y:0, z:-30}, 60.0);
    start_camera.pointAtCoord(400.6378, -1005.109, -99.00404);
    start_camera.setActive(true);

    var end_camera = mp.cameras.new("end", {x: 403.6378, y: -998.5422, z: -99.00404}, {x:0, y:0, z:-30}, 60.0);
    end_camera.pointAtCoord(402.9198, -996.5348, -99.00024);
    end_camera.setActiveWithInterp(start_camera.handle, 5000, 0, 0);
    mp.game.cam.renderScriptCams(true, false, 0, true, false);
});

mp.events.add('CharacterSelectorBrowserReady', () => {
    mp.events.callRemote("RetrieveCharactersList");
});

mp.events.add('SelectCharacterToPlay', (id) => {
    if (browser) {
        browser.destroy();
        browser = null;
    }

    var character = JSON.parse(characters);
    mp.events.callRemote("SelectCharacter", character[id].Id);

    mp.gui.cursor.visible = false;
    mp.gui.chat.activate(true);
    mp.gui.chat.show(true);
    
    mp.game.cam.renderScriptCams(false, false, 0, false, false);
});

mp.events.add('SendToCharacterCreator', () => {
    if (browser) {
        browser.destroy();
        browser = null;
    }
    mp.events.call('ShowCharacterCreator');
});
