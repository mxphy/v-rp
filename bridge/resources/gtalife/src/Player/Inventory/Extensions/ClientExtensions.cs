﻿using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using GTANetworkAPI;
using Newtonsoft.Json;
using gtalife.src.Player.Inventory.Classes;

namespace gtalife.src.Player.Inventory.Extensions
{
    public static class ClientExtensions
    {
        public static void loadInventory(this Client player)
        {
            if (Main.Inventories.ContainsKey(player.Handle)) return;
            List<InventoryItem> items = null;
            string playerFile = Main.SaveLocation + Path.DirectorySeparatorChar + player.Name + ".json";

            if (File.Exists(playerFile)) items = JsonConvert.DeserializeObject<List<InventoryItem>>(File.ReadAllText(playerFile));
            Main.Inventories.Add(player.Handle, items ?? new List<InventoryItem>());
        }

        public static int giveItem(this Client player, ItemID ID, int amount)
        {
            if (!Main.Inventories.ContainsKey(player.Handle) || !ItemDefinitions.ItemDictionary.ContainsKey(ID)) return -1;
            List<InventoryItem> playerInventory = Main.Inventories[player.Handle];
            int stackSize = ItemDefinitions.ItemDictionary[ID].StackSize;
            int added = 0;

            // https://scottlilly.com/how-to-build-stackable-inventory-for-a-game-in-c/
            while (amount > 0)
            {
                if (playerInventory.Exists(i => (i.ID == ID) && (i.Quantity < stackSize)))
                {
                    InventoryItem invItem = playerInventory.First(i => (i.ID == ID) && (i.Quantity < stackSize));
                    int maxQuantity = (stackSize - invItem.Quantity);
                    int toAdd = Math.Min(amount, maxQuantity);

                    invItem.Quantity += toAdd;
                    added += toAdd;
                    amount -= toAdd;
                }
                else
                {
                    if (playerInventory.Count >= Main.MaxInventorySlots)
                    {
                        player.SendChatMessage($"Inventario completo, no se pudo agregar {ID} x{amount}.");
                        break;
                    }

                    playerInventory.Add(new InventoryItem(ID, 0));
                }
            }

            player.TriggerEvent("CloseInventoryMenus");
            return added;
        }

        public static bool hasItem(this Client player, ItemID ID)
        {
            if (!Main.Inventories.ContainsKey(player.Handle) || !ItemDefinitions.ItemDictionary.ContainsKey(ID)) return false;
            return (Main.Inventories[player.Handle].Exists(i => i.ID == ID));
        }

        public static int getItem(this Client player, ItemID ID)
        {
            if (!Main.Inventories.ContainsKey(player.Handle) || !ItemDefinitions.ItemDictionary.ContainsKey(ID)) return -1;
            return (Main.Inventories[player.Handle].FindIndex(i => i.ID == ID));
        }

        public static int getItemAmount(this Client player, ItemID ID)
        {
            if (!Main.Inventories.ContainsKey(player.Handle) || !ItemDefinitions.ItemDictionary.ContainsKey(ID)) return 0;
            return (Main.Inventories[player.Handle].First(i => i.ID == ID).Quantity);
        }

        public static void useItem(this Client player, int index, int quantity = 1)
        {
            if (!Main.Inventories.ContainsKey(player.Handle)) return;
            if (index < 0 || index >= Main.Inventories[player.Handle].Count) return;
            InventoryItem item = Main.Inventories[player.Handle][index];
            
            if (item.Item.Use(player))
            {
                item.Quantity -= quantity;
                if (item.Quantity < 1) Main.Inventories[player.Handle].RemoveAt(index);

                sendInventory(player);
            }
        }

        public static void removeItem(this Client player, int index, int amount)
        {
            if (!Main.Inventories.ContainsKey(player.Handle)) return;
            if (index < 0 || index >= Main.Inventories[player.Handle].Count) return;
            InventoryItem item = Main.Inventories[player.Handle][index];

            item.Quantity -= amount;
            if (item.Quantity < 1) Main.Inventories[player.Handle].RemoveAt(index);

            sendInventory(player);
        }

        public static void sendInventory(this Client player)
        {
            if (!Main.Inventories.ContainsKey(player.Handle)) return;
            List<InventoryItemData> playerInventory = Main.Inventories[player.Handle].Select(i => new InventoryItemData(i)).ToList();
            player.TriggerEvent("ReceiveInventory", NAPI.Util.ToJson(playerInventory));
        }

        public static void clearInventory(this Client player)
        {
            if (!Main.Inventories.ContainsKey(player.Handle)) return;
            Main.Inventories[player.Handle].Clear();
            player.TriggerEvent("CloseInventoryMenus");
        }

        public static void saveInventory(this Client player)
        {
            if (!Main.Inventories.ContainsKey(player.Handle)) return;
            string playerFile = Main.SaveLocation + Path.DirectorySeparatorChar + player.Name + ".json";
            File.WriteAllText(playerFile, JsonConvert.SerializeObject(Main.Inventories[player.Handle], Formatting.Indented));
        }
    }
}
