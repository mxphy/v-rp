﻿using GTANetworkAPI;
using gtalife.src.Gameplay.Boombox;
using gtalife.src.Player.Inventory.Classes;
using gtalife.src.Player.Inventory.Interfaces;
using System.Linq;

namespace gtalife.src.Player.Inventory.ItemTypes
{
    class BoomboxItem : BaseItem, IDroppable
    {
        public WorldModel DropModel { get; set; }

        public BoomboxItem(string name, string description, int stackSize, WorldModel dropModel) : base(name, description, stackSize)
        {
            DropModel = dropModel;
        }

        public override bool Use(Client player)
        {
            if (Gameplay.Boombox.Main.Boomboxes.Where(b => b.Position.DistanceTo(player.Position) <= Gameplay.Boombox.Main.BOOMBOX_RANGE + 5f).Count() > 0)
            {
                player.SendChatMessage("~r~ERROR: ~w~Ya hay un radio cerca.");
                return false;
            }
            else
            {
                var boombox = new Boombox(Gameplay.Boombox.Main.GetGuid(), player.Name, player.Position, new Vector3());
                Gameplay.Boombox.Main.Boomboxes.Add(boombox);

                foreach (var p in NAPI.Pools.GetAllPlayers())
                {
                    if (p.Position.DistanceTo(player.Position) <= Gameplay.Boombox.Main.BOOMBOX_RANGE)
                    {
                        p.SetData("Boombox_ID", boombox.ID);
                        p.TriggerEvent("BoomBox:Enter", NAPI.Util.ToJson(boombox));
                    }
                }

                return true;
            }
        }
    }
}
