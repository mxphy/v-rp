﻿using GTANetworkAPI;
using Newtonsoft.Json;
using System;
using System.IO;

namespace gtalife.src.Gameplay.Boombox
{
    #region Boombox Class
    public class Boombox
    {
        public Guid ID { get; }
        public string Owner { get; private set; }
        public string Genre { get; private set; }
        public string Url { get; set; }
        public bool Enabled { get; set; }
        public Vector3 Position { get; }
        public Vector3 Rotation { get; }

        // entities
        [JsonIgnore]
        private ColShape ColShape;

        [JsonIgnore]
        private TextLabel Label;

        [JsonIgnore]
        private GTANetworkAPI.Object Object;

        [JsonIgnore]
        private DateTime LastSave;

        public Boombox(Guid id, string owner, Vector3 position, Vector3 rotation)
        {
            ID = id;
            Owner = owner;
            Genre = "";
            Url = "";
            Enabled = false;
            Position = position;
            Rotation = rotation;

            // create label
            Label = NAPI.TextLabel.CreateTextLabel("Radio", Position - new Vector3(0.0, 0.0, 0.5), 15f, 0.5f, 1, new Color(255, 255, 255, 255));

            // create object
            Object = NAPI.Object.CreateObject(NAPI.Util.GetHashKey("prop_boombox_01"), Position - new Vector3(0.0, 0.0, 0.85), rotation);

            // create colshape
            ColShape = NAPI.ColShape.CreateCylinderColShape(Position, Main.BOOMBOX_RANGE, Main.BOOMBOX_RANGE);
            ColShape.OnEntityEnterColShape += (s, ent) =>
            {
                Client player;

                if ((player = NAPI.Player.GetPlayerFromHandle(ent)) != null)
                {
                    player.SetData("Boombox_ID", ID);
                    player.TriggerEvent("BoomBox:Enter", NAPI.Util.ToJson(this));
                }
            };

            ColShape.OnEntityExitColShape += (s, ent) =>
            {
                Client player;

                if ((player = NAPI.Player.GetPlayerFromHandle(ent)) != null)
                {
                    player.ResetData("Boombox_ID");
                    player.TriggerEvent("BoomBox:Exit");
                }
            };
        }

        public void SetGenre(string genre)
        {
            Genre = genre;
            if (string.IsNullOrEmpty(genre))
                Label.Text = $"Radio";
            else
                Label.Text = $"Radio~n~Escuchando: ~g~{genre}";
        }

        public void UpdateForAll()
        {
            foreach (var player in NAPI.Pools.GetAllPlayers())
            {
                if (!player.HasData("Boombox_ID") || player.GetData("Boombox_ID") != ID) continue;

                player.TriggerEvent("BoomBox:Enter", NAPI.Util.ToJson(this));
            }
        }

        public void Save(bool force = false)
        {
            if (!force && DateTime.Now.Subtract(LastSave).TotalSeconds < Main.SAVE_INTERVAL) return;

            File.WriteAllText(Main.BOOMBOX_SAVE_DIR + Path.DirectorySeparatorChar + ID + ".json", JsonConvert.SerializeObject(this, Formatting.Indented));
            LastSave = DateTime.Now;
        }

        public void Destroy()
        {
            Label.Delete();
            Object.Delete();
            NAPI.ColShape.DeleteColShape(ColShape);
        }
    }
    #endregion
}
