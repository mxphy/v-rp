﻿using GTANetworkAPI;
using System.Collections.Generic;
using System.Linq;

namespace gtalife.src.Managers
{
    public class DoorManager : Script
    {
        private static int _doorCounter;
        private static Dictionary<int, ColShape> _doorColShapes = new Dictionary<int, ColShape>();

        private bool _debugStatus = true;

        public const ulong SET_STATE_OF_CLOSEST_DOOR_OF_TYPE = 0xF82D8F1926A02C3D;

        [ServerEvent(Event.PlayerEnterColshape)]
        public void OnPlayerEnterColshape(ColShape colshape, Client player)
        {
            if (colshape != null && colshape.GetData("IS_DOOR_TRIGGER") == true)
            {
                var id = colshape.GetData("DOOR_ID");
                var info = colshape.GetData("DOOR_INFO");

                float heading = 0f;

                if (info.State != null) heading = info.State;

                NAPI.Native.SendNativeToPlayer(player, SET_STATE_OF_CLOSEST_DOOR_OF_TYPE,
                    info.Hash, info.Position.X, info.Position.Y, info.Position.Z,
                    info.Locked, heading, false);
            }
        }

        /* EXPORTED METHODS */
        public static int registerDoor(int modelHash, Vector3 position)
        {
            var colShapeId = ++_doorCounter;

            var info = new DoorInfo();
            info.Hash = modelHash;
            info.Position = position;
            info.Locked = false; // Open by default;
            info.Id = colShapeId;
            info.State = 0;

            var colShape = NAPI.ColShape.CreateSphereColShape(position, 35f);
            colShape.SetData("DOOR_INFO", info);
            colShape.SetData("DOOR_ID", colShapeId);
            colShape.SetData("IS_DOOR_TRIGGER", true);

            _doorColShapes.Add(colShapeId, colShape);

            return colShapeId;
        }

        public void transitionDoor(int doorId, float finish, int ms)
        {
            /*if (_doorColShapes.ContainsKey(doorId))
            {
                var info = _doorColShapes[doorId].GetData("DOOR_INFO");

                info.Locked = true;

                foreach (var entity in _doorColShapes[doorId].getAllEntities())
                {
                    var player = NAPI.Player.GetPlayerFromHandle(entity);

                    if (player == null) continue;

                    NAPI.ClientEvent.TriggerClientEvent(player, "doormanager_transitiondoor",
                        info.Hash, info.Position, info.State, finish, ms);
                }

                info.State = finish;
            }*/
        }

        public void refreshDoorState(int doorId)
        {
            /*if (_doorColShapes.ContainsKey(doorId))
            {
                var info = _doorColShapes[doorId].GetData("DOOR_INFO");

                float heading = info.State;

                foreach (var entity in _doorColShapes[doorId].getAllEntities())
                {
                    var player = API.getPlayerFromHandle(entity);

                    if (player == null) continue;

                    NAPI.Native.SendNativeToPlayer(player, SET_STATE_OF_CLOSEST_DOOR_OF_TYPE,
                        info.Hash, info.Position.X, info.Position.Y, info.Position.Z,
                        info.Locked, heading, false);
                }
            }*/
        }

        public void removeDoor(int id)
        {
            if (_doorColShapes.ContainsKey(id))
            {
                NAPI.ColShape.DeleteColShape(_doorColShapes[id]);
                _doorColShapes.Remove(id);
            }
        }

        public static void setDoorState(int doorId, bool locked, float heading)
        {
            /*if (_doorColShapes.ContainsKey(doorId))
            {
                var door = _doorColShapes[doorId];
                var data = door.GetData("DOOR_INFO");
                data.Locked = locked;
                data.State = heading;

                door.SetData("DOOR_INFO", data);

                foreach (var entity in door.getAllEntities())
                {
                    var player = NAPI.Player.GetPlayerFromHandle(entity);

                    if (player == null) continue;

                    float cH = data.State;

                    NAPI.Native.SendNativeToPlayer(player, SET_STATE_OF_CLOSEST_DOOR_OF_TYPE,
                        data.Hash, data.Position.X, data.Position.Y, data.Position.Z,
                        data.Locked, cH, false);
                }
            }*/
        }

        public int getCloseDoor(Client player)
        {
            /*var localCopy = new Dictionary<int, ColShape>(_doorColShapes);
            return localCopy.FirstOrDefault(pair => pair.Value.containsEntity(player)).Key;*/
            return 0;
        }

        public int[] getAllCloseDoors(Client player)
        {
            var localCopy = new Dictionary<int, ColShape>(_doorColShapes);
            var list = new List<int>();
            /*foreach (var sh in localCopy)
            {
                if (sh.Value.containsEntity(player))
                    list.Add(sh.Key);
            }*/

            return list.ToArray();
        }

        public void setDebug(bool status)
        {
            _debugStatus = status;
        }
    }

    public struct DoorInfo
    {
        public int Hash;
        public Vector3 Position;
        public int Id;

        public bool Locked;
        public float State;
    }
}
