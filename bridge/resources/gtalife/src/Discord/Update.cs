﻿using GTANetworkAPI;
using gtalife.src.Managers;
using gtalife.src.Player.Utils;
using System;
using DSharpPlus.Entities;

namespace gtalife.src.Discord
{
    class Update : Script
    {
        public DateTime LastAnnounce;

        [ServerEvent(Event.Update)]
        public void OnUpdate()
        {
            if (DateTime.Now.Subtract(LastAnnounce).TotalSeconds >= 10)
            {
                LastAnnounce = DateTime.Now;
                if (Main.PlayerListMessage != null)
                {
                    string message = "";
                    int playerCount = 0;

                    foreach (var player in NAPI.Pools.GetAllPlayers())
                    {
                        if (!player.IsLogged()) continue;

                        playerCount++;
                        message += $"```{player.Name} | Level: {Player.Data.Character[player].Level} | ID: {PlayeridManager.GetPlayerId(player)}```\n\n";
                    }

                    if (playerCount > 0)
                    {
                        message = message.Insert(0, $"**Lista de jugadores conectados en el servidor ({playerCount})**\n\n Versión 0.3.1 — www.rage.mp");

                        var embed = new DiscordEmbedBuilder
                        {
                            Title = "Jugadores en linea",
                            Description = $"**Hay {playerCount} jugadores conectados en el servidor**\n\n Versión 0.3.1 — www.rage.mp",
                            Color = new DiscordColor(0x888888)
                        };
                        Main.PlayerListMessage.ModifyAsync(embed: embed);
                    }
                    else
                    {
                        var embed = new DiscordEmbedBuilder
                        {
                            Title = "Jugadores en linea",
                            Description = $"**No hay jugadores en línea en este momento** \n\n Versión 0.3.1 — www.rage.mp",
                            Color = new DiscordColor(0x888888)
                        };
                        Main.PlayerListMessage.ModifyAsync(embed: embed);
                    }
                }
            }
        }
    }
}

