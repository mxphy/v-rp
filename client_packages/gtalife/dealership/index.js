const NativeUI = require("nativeui");
const Menu = NativeUI.Menu;
const UIMenuItem = NativeUI.UIMenuItem;
const Point = NativeUI.Point;
const Offset = require("gtalife/util/offsets.js")

let is_on_vehicle = false;
let dealership_purchase_menu = null;
let colors = [
    { id: 0, name: "Black" },
    { id: 4, name: "Silver" },
    { id: 27, name: "Red" },
    { id: 36, name: "Orange" },
    { id: 37, name: "Gold" },
    { id: 49, name: "Green" },
    { id: 64, name: "Blue" },
    { id: 88, name: "Yellow" },
    { id: 120, name: "Chrome" },
    { id: 134, name: "White" }
];

let color1 = 0;
let color2 = 0;

function d_numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

mp.events.add('OnEnterDealershipVehicle', () => {
    is_on_vehicle = true;
});

mp.events.add('OnExitDealershipVehicle', () => {
    is_on_vehicle = false;
});

mp.events.add('Dealership_PurchaseMenu', () => {
    let data = JSON.parse(args);

    if (dealership_purchase_menu == null) {
        dealership_purchase_menu = new Menu("Concesionario", "Este vehículo esta en venta!", new Point(mp.game.resolution.width - Offset.getWidthOffset(mp.game.resolution.width), 50));

        dealership_purchase_menu.ItemSelect.on((item, index) => {
            if (index == 3) mp.events.callRemote("VehiclePurchase", colors[color1].id, colors[color2].id);
            dealership_purchase_menu.Visible = false;
        });
    }

    dealership_purchase_menu.Clear();

    let temp_item = new UIMenuItem("Precio", "Precio de lo vehículo.");
    temp_item.SetRightLabel("$" + d_numberWithCommas(data.Price));
    dealership_purchase_menu.AddItem(temp_item);

    let list = [];
    for (let i = 0, len = colors.length; i < len; i++) list.push(colors[i].name);

    let list1 = new UIMenuListItem("Color 1", "Seleccione el color del vehículo.", new ItemsCollection(list))
    dealership_purchase_menu.AddItem(list1);        

    let list2 = new UIMenuListItem("Color 2", "Seleccione el color del vehículo.", new ItemsCollection(list))
    dealership_purchase_menu.AddItem(list2);

    dealership_purchase_menu.ListChange.on((item, index) => {            
        if (item == list1) color1 = index;
        else if (item == list2) color2 = index;
    });

    temp_item = new UIMenuItem("Comprar", "Seleccione esta opción para comprar lo vehículo.");
    dealership_purchase_menu.AddItem(temp_item);

    dealership_purchase_menu.Visible = true;
});

// E key
mp.keys.bind(0x45, false, () => {
    if (mp.gui.cursor.visible) return;
    
    if (is_on_vehicle) mp.events.callRemote("RequestDealershipBuyMenu");
});
