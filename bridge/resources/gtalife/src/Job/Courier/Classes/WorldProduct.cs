﻿using System;
using GTANetworkAPI;

namespace gtalife.src.Job.Courier
{
    public class WorldProduct
    {
        public Guid ID { get; set; }
        public ProductType Type { get; set; }
        public Vector3 Position { get; set; }
        public Vector3 Rotation { get; set; }
        public DateTime SpawnTime { get; set; }

        // Entities
        public GTANetworkAPI.Object Object { get; set; }
        public TextLabel Label { get; set; }

        public WorldProduct(ProductType type, NetHandle? objHandle, Vector3 position, Vector3 rotation)
        {
            ID = Methods.GenerateProductGuid();
            Type = type;
            Position = position;
            Rotation = rotation;
            SpawnTime = DateTime.Now;

            // Create entities
            Object = (objHandle == null) ? NAPI.Object.CreateObject(NAPI.Util.GetHashKey(ProductDefinitions.Products[type].PropName), position, rotation) : NAPI.Entity.GetEntityFromHandle<GTANetworkAPI.Object>(objHandle.Value);

            // Existing object, detach it and set position
            if (NAPI.Entity.IsEntityAttachedToAnything(Object.Handle))
            {
                Object.Detach();
                Object.Position = position;
                Object.Rotation = rotation;
            }

            Object.SetSharedData("Courier_DetectorID", ID.ToString());
            NAPI.Native.SendNativeToPlayersInRange(position, 300f, Hash.SET_ENTITY_PROOFS, Object.Handle, true, true, true, true, true, true, 1, true);

            Label = NAPI.TextLabel.CreateTextLabel($"{type}", position + new Vector3(0.0, 0.0, 0.5), 10f, 0.5f, 1, new Color(255, 255, 255, 255), true);
        }

        public void DeleteEntities(bool deleteObj = true)
        {
            if (deleteObj) Object?.Delete();
            Label?.Delete();
        }
    }
}
