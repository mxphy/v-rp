﻿using GTANetworkAPI;

namespace gtalife.src.Player.Utils
{
    public class RemoteEvents : Script
    {
        [RemoteEvent("SetPlayerPos")]
        public void SetPlayerPos(Client player, object[] arguments)
        {
            if (arguments.Length < 3) return;
            NAPI.Entity.SetEntityPosition(player.Handle, new Vector3((float)arguments[0], (float)arguments[1], (float)arguments[2]));

            bool freeze = arguments.Length == 4 ? (bool)arguments[3] : false;
            if (freeze) NAPI.Player.FreezePlayer(player, freeze);
        }

        [RemoteEvent("SetPlayerRot")]
        public void SetPlayerRot(Client player, object[] arguments)
        {
            if (arguments.Length < 1) return;
            NAPI.Entity.SetEntityRotation(player.Handle, new Vector3(0f, 0f, (float)arguments[0]));
        }

        [RemoteEvent("SetPlayerSkin")]
        public void SetPlayerSkin(Client player, string pedName)
        {
            NAPI.Player.SetPlayerSkin(player, NAPI.Util.PedNameToModel(pedName));
        }

        [RemoteEvent("SetPlayerClothes")]
        public void SetPlayerClothes(Client player, int slot, int drawable, int texture)
        {
            NAPI.Player.SetPlayerClothes(player, slot, drawable, texture);
        }

        [RemoteEvent("SetPlayerAccessory")]
        public void SetPlayerAccessory(Client player, int slot, int drawable, int texture)
        {
            NAPI.Player.SetPlayerAccessory(player, slot, drawable, texture);
        }
    }
}
