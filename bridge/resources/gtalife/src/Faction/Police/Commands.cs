﻿using GTANetworkAPI;
using gtalife.src.Flags;
using gtalife.src.Managers;
using gtalife.src.Player.Outfit;
using gtalife.src.Player.Utils;
using System;
using System.Collections.Generic;
using System.Linq;

namespace gtalife.src.Faction.Police
{
    class Commands : Script
    {
        Dictionary<Client, Client> PlayersCuffed = new Dictionary<Client, Client>();
        public static Vector3[] CellPositions = new[]
        {
            new Vector3(459.6251, -1001.719, 24.91486),
            new Vector3(459.4262, -998.1058, 24.91486),
            new Vector3(459.8662, -993.8979, 24.91486)
        };

        [Command("megafono", Alias = "m", GreedyArg = true)]
        public void MegaphoneCommand(Client player, string message)
        {
            Faction faction = Main.Factions.FirstOrDefault(f => f.ID == player.GetFactionID());
            if (faction == null || (faction.Type != (int)FactionType.Police && faction.Type != (int)FactionType.Army)) NAPI.Chat.SendChatMessageToPlayer(player, "~r~ERROR: ~w~No tienes permiso.");
            else if (!Police.IsOnDuty(player)) NAPI.Chat.SendChatMessageToPlayer(player, "~r~ERROR: ~w~No estás de servicio.");
            else if (!player.IsInVehicle) NAPI.Chat.SendChatMessageToPlayer(player, "~r~ERROR: ~w~No estás en un carro de policía.");
            else if (!Police.PoliceVehicles.Contains((VehicleHash)player.Vehicle.Model)) NAPI.Chat.SendChatMessageToPlayer(player, "~r~ERROR: ~w~No estás en un carro de policía.");
            else
            {
                var players = player.getPlayersInRadiusOfPlayer(30f);
                foreach (var p in players) p.SendChatMessage($"!{{#ffff00}}[MEGÁFONO] {faction.Ranks[(int)player.GetFactionRank()]} {player.Name}: {message}");
            }
        }

        [Command("multar", "~y~USO: ~w~/multar [jugador] [dinero]")]
        public void FineCommand(Client player, string idOrName, int money)
        {
            var target = PlayeridManager.FindPlayer(idOrName);
            Faction faction = Main.Factions.FirstOrDefault(f => f.ID == player.GetFactionID());

            if (target == null) player.SendChatMessage("~r~ERROR: ~w~Demasiados o ningún jugador encontrado.");
            else if (!target.IsLogged()) player.SendChatMessage("~r~ERROR: ~w~El jugador no está conectado.");
            else if (faction == null || (faction.Type != (int)FactionType.Police && faction.Type != (int)FactionType.Army)) NAPI.Chat.SendChatMessageToPlayer(player, "~r~ERROR: ~w~No tienes permiso.");
            else if (!Police.IsOnDuty(player)) NAPI.Chat.SendChatMessageToPlayer(player, "~r~ERROR: ~w~No estás de servicio.");
            else if (target.Position.DistanceTo(player.Position) > 2f) NAPI.Chat.SendChatMessageToPlayer(player, "~r~ERROR: ~w~No estás cerca del jugador.");
            else if (player == target) NAPI.Chat.SendChatMessageToPlayer(player, "~r~ERROR: ~w~No puedes hacer eso.");
            else if (money < 1) NAPI.Chat.SendChatMessageToPlayer(player, "~r~ERROR: ~w~El dinero debe estar arriba de 0.");
            else
            {
                target.SendChatMessage($"~y~INFO: ~w~{player.Name} te está multando por ~y~${money} ~w~use ~y~/pagarmulta~w~ para pagar.");
                player.SendChatMessage($"~y~INFO: ~w~Usted está multando {target.Name} por ~y~${money}.");
                player.sendChatAction($"da una multa a {target.Name}.");

                target.SetData("FINE_PRICE", money);
                target.SetData("FINE_PLAYER", player);
            }
        }

        [Command("pagarmulta")]
        public void PayFineCommand(Client player)
        {
            if (!player.HasData("FINE_PRICE"))
            {
                player.SendChatMessage("~r~ERROR: ~w~Nadie te dio una multa.");
                return;
            }

            int money = player.GetData("FINE_PRICE");
            Client target = player.GetData("FINE_PLAYER");

            if (money > player.getMoney()) NAPI.Chat.SendChatMessageToPlayer(player, "~r~ERROR: ~w~No tienes suficiente dinero.");
            else if (target.Position.DistanceTo(player.Position) > 2f) NAPI.Chat.SendChatMessageToPlayer(player, "~r~ERROR: ~w~No estás cerca del jugador.");
            else
            {
                target.SendChatMessage($"~y~INFO: ~w~{player.Name} pagó la multa.");
                player.SendChatMessage($"~y~INFO: ~w~Usted pagó la multa.");
                player.sendChatAction($"pagó la multa.");

                player.giveMoney(-money);
                target.giveMoney(money);

                player.ResetData("FINE_PRICE");
                player.ResetData("FINE_PLAYER");
            }
        }

        [Command("cuff", GreedyArg = true)]
        public void CuffCommand(Client player, string idOrName)
        {
            var target = PlayeridManager.FindPlayer(idOrName);
            Faction faction = Main.Factions.FirstOrDefault(f => f.ID == player.GetFactionID());

            if (target == null) player.SendChatMessage("~r~ERROR: ~w~Demasiados o ningún jugador encontrado.");
            else if (!target.IsLogged()) player.SendChatMessage("~r~ERROR: ~w~El jugador no está conectado.");
            else if (faction == null || (faction.Type != (int)FactionType.Police && faction.Type != (int)FactionType.Army)) NAPI.Chat.SendChatMessageToPlayer(player, "~r~ERROR: ~w~No tienes permiso.");
            else if (!Police.IsOnDuty(player)) NAPI.Chat.SendChatMessageToPlayer(player, "~r~ERROR: ~w~No estás de servicio.");
            else if (player.IsInVehicle) NAPI.Chat.SendChatMessageToPlayer(player, "~r~ERROR: ~w~No puedes hacer eso dentro de un vehículo.");
            else if (target.Position.DistanceTo(player.Position) > 2f) NAPI.Chat.SendChatMessageToPlayer(player, "~r~ERROR: ~w~No estás cerca del jugador.");
            else if (target.IsInVehicle) NAPI.Chat.SendChatMessageToPlayer(player, "~r~ERROR: ~w~El jugador está en un vehículo.");
            else if (player == target) NAPI.Chat.SendChatMessageToPlayer(player, "~r~ERROR: ~w~No puedes hacer eso.");
            else if (PlayersCuffed.ContainsKey(player)) NAPI.Chat.SendChatMessageToPlayer(player, "~r~ERROR: ~w~Ya estás sosteniendo a alguien.");
            else if (PlayersCuffed.ContainsValue(target)) NAPI.Chat.SendChatMessageToPlayer(player, "~r~ERROR: ~w~Este jugador ya está esposado.");
            else
            {
                player.sendChatAction($"intenta esposar {target.Name}.");
                NAPI.Task.Run(() =>
                {
                    if (PlayersCuffed.ContainsKey(player)) return;

                    if (player.Position.DistanceTo(target.Position) > 2f)
                        player.sendChatAction($"falló al esposar {target.Name}.");
                    else
                    {
                        PlayersCuffed.Add(player, target);
                        target.TriggerEvent("SetPlayerCuffed", true);
                        player.sendChatAction($"esposa {target.Name} y se agarra del brazo.");
                        NAPI.Player.PlayPlayerAnimation(target, (int)(AnimationFlags.Loop | AnimationFlags.OnlyAnimateUpperBody | AnimationFlags.AllowPlayerControl), "mp_arresting", "idle");

                        var cuff = NAPI.Object.CreateObject(-1281059971, new Vector3(0.0f, 0.0f, 0.0f), new Vector3(0.0f, 0.0f, 0.0f));
                        NAPI.Entity.AttachEntityToEntity(cuff, target, "IK_R_Hand", new Vector3(-0.02f, 0.07f, 0f), new Vector3(90f, 90f, -10f));
                        target.SetData("CUFF_OBJECT", cuff);
                    }
                }, delayTime: 2000);
            }
        }

        [Command("uncuff", GreedyArg = true)]
        public void UncuffCommand(Client player, string idOrName)
        {
            var target = PlayeridManager.FindPlayer(idOrName);
            Faction faction = Main.Factions.FirstOrDefault(f => f.ID == player.GetFactionID());

            if (target == null) player.SendChatMessage("~r~ERROR: ~w~Demasiados o ningún jugador encontrado.");
            else if (!target.IsLogged()) player.SendChatMessage("~r~ERROR: ~w~El jugador no está conectado.");
            else if (faction == null || (faction.Type != (int)FactionType.Police && faction.Type != (int)FactionType.Army)) NAPI.Chat.SendChatMessageToPlayer(player, "~r~ERROR: ~w~No tienes permiso.");
            else if (!Police.IsOnDuty(player)) NAPI.Chat.SendChatMessageToPlayer(player, "~r~ERROR: ~w~No estás de servicio.");
            else if (player.IsInVehicle) NAPI.Chat.SendChatMessageToPlayer(player, "~r~ERROR: ~w~No puedes hacer eso dentro de un vehículo.");
            else if (target.Position.DistanceTo(player.Position) > 2f) NAPI.Chat.SendChatMessageToPlayer(player, "~r~ERROR: ~w~No estás cerca del jugador.");
            else if (target.IsInVehicle) NAPI.Chat.SendChatMessageToPlayer(player, "~r~ERROR: ~w~El jugador está en un vehículo.");
            else if (player == target) NAPI.Chat.SendChatMessageToPlayer(player, "~r~ERROR: ~w~No puedes hacer eso.");
            else if (!PlayersCuffed.ContainsValue(target)) NAPI.Chat.SendChatMessageToPlayer(player, "~r~ERROR: ~w~Este jugador no está esposado.");
            else
            {
                var police = PlayersCuffed.FirstOrDefault(x => x.Value == target).Key;
                PlayersCuffed.Remove(police);

                player.sendChatAction($"desposa {target.Name}.");

                target.StopAnimation();

                target.TriggerEvent("SetPlayerCuffed", false);
                NAPI.Entity.DeleteEntity(target.GetData("CUFF_OBJECT"));
                target.ResetData("CUFF_OBJECT");
            }
        }

        [Command("detener", "~y~USO: ~w~/detener [jugador] [tiempo(min)]")]
        public void ArrestCommand(Client player, string idOrName, int time)
        {
            var target = PlayeridManager.FindPlayer(idOrName);
            Faction faction = Main.Factions.FirstOrDefault(f => f.ID == player.GetFactionID());

            if (target == null) player.SendChatMessage("~r~ERROR: ~w~Demasiados o ningún jugador encontrado.");
            else if (!target.IsLogged()) player.SendChatMessage("~r~ERROR: ~w~El jugador no está conectado.");
            else if (faction == null || faction.Type != (int)FactionType.Police) NAPI.Chat.SendChatMessageToPlayer(player, "~r~ERROR: ~w~No tienes permiso.");
            else if (!Police.IsOnDuty(player)) NAPI.Chat.SendChatMessageToPlayer(player, "~r~ERROR: ~w~No estás de servicio.");
            else if (time < 1 || time > 20) NAPI.Chat.SendChatMessageToPlayer(player, "~r~ERROR: ~w~Tiempo debe estar entre 1 y 20 minutos.");
            else if (player.Position.DistanceTo(target.Position) > 2f) NAPI.Chat.SendChatMessageToPlayer(player, "~r~ERROR: ~w~Estás demasiado lejos del jugador.");
            else if (player.Position.DistanceTo(new Vector3(464.0663, -997.5513, 24.91487)) > 8f) NAPI.Chat.SendChatMessageToPlayer(player, "~r~ERROR: ~w~No estás cerca de las celdas de la prisión.");
            else
            {
                Random rand = new Random();

                int i = rand.Next(CellPositions.Length);
                target.Position = CellPositions[i];

                Player.Data.Character[target].JailTime = time * 60;
                Player.Data.Character[target].WantedLevel = 0;

                target.WantedLevel = 0;
                target.RemoveAllWeapons();
                target.SetOutfit(67);

                player.sendChatAction($"ha detenido {target.Name}");
            }
        }

        [Command("soltar", "~y~USO: ~w~/soltar [jugador]", GreedyArg = true)]
        public void ReleaseCommand(Client player, string idOrName)
        {
            var target = PlayeridManager.FindPlayer(idOrName);
            Faction faction = Main.Factions.FirstOrDefault(f => f.ID == player.GetFactionID());

            if (target == null) player.SendChatMessage("~r~ERROR: ~w~Demasiados o ningún jugador encontrado.");
            else if (!target.IsLogged()) player.SendChatMessage("~r~ERROR: ~w~El jugador no está conectado.");
            else if (faction == null || faction.Type != (int)FactionType.Police) NAPI.Chat.SendChatMessageToPlayer(player, "~r~ERROR: ~w~No tienes permiso.");
            else if (!Police.IsOnDuty(player)) NAPI.Chat.SendChatMessageToPlayer(player, "~r~ERROR: ~w~No estás de servicio.");
            else if (player.Position.DistanceTo(target.Position) > 2f) NAPI.Chat.SendChatMessageToPlayer(player, "~r~ERROR: ~w~Estás demasiado lejos del jugador.");
            else
            {
                if (Player.Data.Character[target].JailTime < 1) player.SendChatMessage("~r~ERROR: ~w~El jugador no está detenido.");
                else
                {
                    Player.Data.Character[target].JailTime = 0;

                    target.Position = new Vector3(444.7051, -988.0085, 30.6896);
                    target.Rotation = new Vector3(0, 0, 7.987312);
                    target.loadClothes();

                    player.sendChatAction($"ha liberado {target.Name}");
                }
            }
        }

        [Command("detenidos")]
        public void ArrestedListCommand(Client player)
        {
            Faction faction = Main.Factions.FirstOrDefault(f => f.ID == player.GetFactionID());
            if (faction == null || (faction.Type != (int)FactionType.Police && faction.Type != (int)FactionType.Army)) NAPI.Chat.SendChatMessageToPlayer(player, "~r~ERROR: ~w~No tienes permiso.");
            else if (!Police.IsOnDuty(player)) NAPI.Chat.SendChatMessageToPlayer(player, "~r~ERROR: ~w~No estás de servicio.");
            else
            {
                List<string> players = new List<string>();
                foreach (var p in NAPI.Pools.GetAllPlayers())
                {
                    if (!p.IsLogged()) continue;
                    if (Player.Data.Character[p].JailTime < 1) continue;

                    players.Add(p.Name);
                }

                if (players.Count < 1) player.SendChatMessage("~y~INFO: ~w~No hay jugadores arrestados en este momento.");
                else player.TriggerEvent("Police_ShowMenuList", NAPI.Util.ToJson(players), "Detenidos");
            }
        }

        [Command("sospechar", "~y~USO: ~w~/sospechar [jugador] [razón]", Alias = "so", GreedyArg = true)]
        public void SuspectCommand(Client player, string idOrName, string reason)
        {
            var target = PlayeridManager.FindPlayer(idOrName);
            Faction faction = Main.Factions.FirstOrDefault(f => f.ID == player.GetFactionID());

            if (target == null) player.SendChatMessage("~r~ERROR: ~w~Demasiados o ningún jugador encontrado.");
            else if (!target.IsLogged()) player.SendChatMessage("~r~ERROR: ~w~El jugador no está conectado.");
            else if (faction == null || (faction.Type != (int)FactionType.Police && faction.Type != (int)FactionType.Army)) NAPI.Chat.SendChatMessageToPlayer(player, "~r~ERROR: ~w~No tienes permiso.");
            else if (!Police.IsOnDuty(player)) NAPI.Chat.SendChatMessageToPlayer(player, "~r~ERROR: ~w~No estás de servicio.");
            else if (player == target) NAPI.Chat.SendChatMessageToPlayer(player, "~r~ERROR: ~w~No puedes hacer eso.");
            else
            {
                Player.Data.Character[target].WantedLevel++;
                target.WantedLevel++;

                Main.SendFactionTypeMessage((FactionType)faction.Type, $"~b~[{faction.Name}] {player.Name} sospechaba de {target.Name}. Razón: {reason}");
                target.SendChatMessage($"!{{#F9C2D1}}* [{faction.Name}] {player.Name} sospechaba de usted. Razón: {reason}");
            }
        }

        [Command("sospechosos")]
        public void SuspectListCommand(Client player)
        {
            Faction faction = Main.Factions.FirstOrDefault(f => f.ID == player.GetFactionID());
            if (faction == null || (faction.Type != (int)FactionType.Police && faction.Type != (int)FactionType.Army)) NAPI.Chat.SendChatMessageToPlayer(player, "~r~ERROR: ~w~No tienes permiso.");
            else if (!Police.IsOnDuty(player)) NAPI.Chat.SendChatMessageToPlayer(player, "~r~ERROR: ~w~No estás de servicio.");
            else
            {
                List<string> players = new List<string>();
                foreach (var p in NAPI.Pools.GetAllPlayers())
                {
                    if (!p.IsLogged()) continue;
                    if (Player.Data.Character[p].WantedLevel < 1) continue;

                    players.Add(p.Name);
                }

                if (players.Count < 1) player.SendChatMessage("~y~INFO: ~w~No hay jugadores sospechosos en este momento.");
                else player.TriggerEvent("Police_ShowMenuList", NAPI.Util.ToJson(players), "Sospechosos");
            }
        }

        [Command("limpiarregistro")]
        public void ClearWantedLevelCommand(Client player, string idOrName)
        {
            var target = PlayeridManager.FindPlayer(idOrName);
            Faction faction = Main.Factions.FirstOrDefault(f => f.ID == player.GetFactionID());

            if (target == null) player.SendChatMessage("~r~ERROR: ~w~Demasiados o ningún jugador encontrado.");
            else if (!target.IsLogged()) player.SendChatMessage("~r~ERROR: ~w~El jugador no está conectado.");
            else if (faction == null || (faction.Type != (int)FactionType.Police && faction.Type != (int)FactionType.Army)) NAPI.Chat.SendChatMessageToPlayer(player, "~r~ERROR: ~w~No tienes permiso.");
            else if (!Police.IsOnDuty(player)) NAPI.Chat.SendChatMessageToPlayer(player, "~r~ERROR: ~w~No estás de servicio.");
            else
            {
                if (Player.Data.Character[target].WantedLevel > 0) 
                {
                    Player.Data.Character[target].WantedLevel = 0;
                    target.WantedLevel = 0;

                    Main.SendFactionTypeMessage((FactionType)faction.Type, $"~b~[SASD] {player.Name} limpiou el registro de {target.Name}.");
                    target.SendChatMessage($"!{{#F9C2D1}}* [SASD] {player.Name} limpiou su registro.");
                }
                else player.SendChatMessage("~r~ERROR: ~w~El jugador no esta sospechoso.");
            }
        }

        [ServerEvent(Event.PlayerSpawn)]
        public void OnPlayerSpawn(Client player)
        {            
            if (player.getJailtime() > 0)
            {
                Random rand = new Random();
                int i = rand.Next(CellPositions.Length);
                player.Position = CellPositions[i];
            }
        }

        [ServerEvent(Event.PlayerDeath)]
        public void OnPlayerDeath(Client player, Client killer, uint reason)
        {
            if (PlayersCuffed.ContainsKey(player))
            {
                var target = PlayersCuffed[player];
                target.sendChatAction($"se escapó de las esposas.");
                target.StopAnimation();

                target.TriggerEvent("SetPlayerCuffed", false);
                NAPI.Entity.DeleteEntity(target.GetData("CUFF_OBJECT"));
                target.ResetData("CUFF_OBJECT");

                PlayersCuffed.Remove(player);
            }
            else if (PlayersCuffed.ContainsValue(player))
            {
                var police = PlayersCuffed.FirstOrDefault(x => x.Value == player).Key;
                PlayersCuffed.Remove(police);

                player.TriggerEvent("SetPlayerCuffed", false);
                NAPI.Entity.DeleteEntity(player.GetData("CUFF_OBJECT"));
                player.ResetData("CUFF_OBJECT");

                Player.Data.Character[player].JailTime = 600;
                Player.Data.Character[player].WantedLevel = 0;

                player.WantedLevel = 0;
                player.SendChatMessage("~y~INFO: ~w~Usted fue abatido mientras era esposado, usted será llevado a la cárcel.");
            }
        }

        [ServerEvent(Event.PlayerDisconnected)]
        public void OnPlayerDisconnected(Client player, DisconnectionType type, string reason)
        {
            if (PlayersCuffed.ContainsKey(player))
            {
                var target = PlayersCuffed[player];
                target.sendChatAction($"se escapó de las esposas.");
                target.StopAnimation();

                target.TriggerEvent("SetPlayerCuffed", false);
                NAPI.Entity.DeleteEntity(target.GetData("CUFF_OBJECT"));
                target.ResetData("CUFF_OBJECT");

                PlayersCuffed.Remove(player);
            }
            else if (PlayersCuffed.ContainsValue(player))
            {
                var police = PlayersCuffed.FirstOrDefault(x => x.Value == player).Key;
                PlayersCuffed.Remove(police);

                NAPI.Entity.DeleteEntity(player.GetData("CUFF_OBJECT"));
                player.ResetData("CUFF_OBJECT");
            }
        }

        public DateTime LastUpdate;
        [ServerEvent(Event.Update)]
        public void OnUpdate()
        {
            if (DateTime.Now.Subtract(LastUpdate).TotalSeconds >= 1)
            {
                LastUpdate = DateTime.Now;

                foreach (var player in NAPI.Pools.GetAllPlayers())
                {
                    if (!player.IsLogged()) continue;

                    if (PlayersCuffed.ContainsKey(player))
                    {
                        Client target = PlayersCuffed[player];
                        if (player.Position.DistanceTo(target.Position) > 30f)
                        {
                            target.StopAnimation();
                            target.sendChatAction("se liberto.");

                            target.TriggerEvent("SetPlayerCuffed", false);
                            NAPI.Entity.DeleteEntity(target.GetData("CUFF_OBJECT"));
                            target.ResetData("CUFF_OBJECT");

                            PlayersCuffed.Remove(player);
                        }
                        else if (target.IsInVehicle || player.IsInVehicle) continue;
                        else if (player.Position.DistanceTo(target.Position) < 5f) continue;
                        else
                        {
                            target.MovePosition(player.Position, 1000);
                            NAPI.ClientEvent.TriggerClientEvent(target, "DisplaySubtitle", $"~r~Esposado", 1000);
                        }
                    }

                    if (Player.Data.Character[player].JailTime > 0)
                    {
                        int minutesLeft = Player.Data.Character[player].JailTime / 60;
                        int secondsLeft = Player.Data.Character[player].JailTime % 60;

                        NAPI.ClientEvent.TriggerClientEvent(player, "DisplaySubtitle", $"Tiempo restante: {minutesLeft.ToString("D2")}:{secondsLeft.ToString("D2")}", 1000);

                        Player.Data.Character[player].JailTime--;

                        // Release player
                        if (Player.Data.Character[player].JailTime == 0)
                        {
                            player.sendChatAction("terminó su oración y fue liberado.");
                            player.Position = new Vector3(444.7051, -988.0085, 30.6896);
                            player.Rotation = new Vector3(0, 0, 7.987312);
                            player.loadClothes();
                        }
                    }
                }
            }
        }
    }
}
