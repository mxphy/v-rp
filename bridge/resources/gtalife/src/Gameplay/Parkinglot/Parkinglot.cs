﻿using GTANetworkAPI;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Collections.Generic;
using System.IO;

namespace gtalife.src.Gameplay.Parkinglot
{
    public class ParkingSpace
    {
        public Vector3 Position { get; set; }

        public Vector3 Rotation { get; set; }
    }

    #region Parkinglot Class
    public class Parkinglot
    {
        public Guid ID { get; }
        public Vector3 Position { get; }
        public List<ParkingSpace> Spawns { get; private set; }

        // storage
        public long Money { get; private set; }

        // entities
        [JsonIgnore]
        private Blip Blip;

        [JsonIgnore]
        private Marker Marker;

        [JsonIgnore]
        private ColShape ColShape;

        [JsonIgnore]
        private TextLabel Label;

        [JsonIgnore]
        private DateTime LastSave;

        public Parkinglot(Guid id, Vector3 position, long money = 0, List<ParkingSpace> spawns = null)
        {
            ID = id;
            Position = position;
            Spawns = spawns ?? new List<ParkingSpace>();
            Money = money;

            // create blip
            Blip = NAPI.Blip.CreateBlip(position);
            Blip.Name = "Estacionamiento";
            Blip.Scale = 1f;
            Blip.ShortRange = true;
            UpdateBlip();

            // create colshape
            ColShape = NAPI.ColShape.CreateCylinderColShape(position, 0.85f, 0.85f);
            ColShape.OnEntityEnterColShape += (s, ent) =>
            {
                Client player;

                if ((player = NAPI.Player.GetPlayerFromHandle(ent)) != null)
                {
                    player.SetData("ParkinglotMarker_ID", ID);
                }
            };

            ColShape.OnEntityExitColShape += (s, ent) =>
            {
                Client player;

                if ((player = NAPI.Player.GetPlayerFromHandle(ent)) != null)
                {
                    player.ResetData("ParkinglotMarker_ID");
                }
            };

            // create marker
            Marker = NAPI.Marker.CreateMarker(1, position - new Vector3(0.0, 0.0, 1.0), new Vector3(), new Vector3(), 1, new Color(0, 255, 165, 150));

            // create text label
            Label = NAPI.TextLabel.CreateTextLabel("~o~Estacionamiento~s~\nPulsa F3 ~s~para usar\n~g~$100", position, 15f, 0.65f, 1, new Color(255, 255, 255, 255));
        }

        private void UpdateBlip()
        {
            Blip.Sprite = 50;
            Blip.Color = 47;
        }

        public void GiveMoney(int amount)
        {
            Money += amount;

            Save();
        }

        public void SetMoney(int amount)
        {
            Money = amount;

            Save();
        }

        public void Save(bool force = false)
        {
            if (!force && DateTime.Now.Subtract(LastSave).TotalSeconds < Main.SAVE_INTERVAL) return;

            File.WriteAllText(Main.PARKINGLOT_SAVE_DIR + Path.DirectorySeparatorChar + ID + ".json", JsonConvert.SerializeObject(this, Formatting.Indented));
            LastSave = DateTime.Now;
        }

        public void Destroy()
        {
            Blip.Delete();
            Marker.Delete();
            NAPI.ColShape.DeleteColShape(ColShape);
            Label.Delete();
        }
    }
    #endregion

    public class Main : Script
    {
        // settings
        public static string PARKINGLOT_SAVE_DIR = "data/Parkinglot";
        public static int PLAYER_PARKINGLOT_LIMIT = 0;
        public static int PARKINGLOT_MONEY_LIMIT = 5000000;
        public static int SAVE_INTERVAL = 120;

        public static List<Parkinglot> Parkinglots = new List<Parkinglot>();

        #region Methods
        public static Guid GetGuid()
        {
            Guid new_guid;

            do
            {
                new_guid = Guid.NewGuid();
            } while (Parkinglots.Count(h => h.ID == new_guid) > 0);

            return new_guid;
        }
        #endregion

        #region Events
        [ServerEvent(Event.ResourceStart)]
        public void OnResourceStart()
        {
            // load settings
            if (NAPI.Resource.HasSetting(this, "parkinglotDirName")) PARKINGLOT_SAVE_DIR = NAPI.Resource.GetSetting<string>(this, "parkinglotDirName");

            PARKINGLOT_SAVE_DIR = NAPI.Resource.GetResourceFolder(this) + Path.DirectorySeparatorChar + PARKINGLOT_SAVE_DIR;
            if (!Directory.Exists(PARKINGLOT_SAVE_DIR)) Directory.CreateDirectory(PARKINGLOT_SAVE_DIR);

            if (NAPI.Resource.HasSetting(this, "playerParkinglotLimit")) PLAYER_PARKINGLOT_LIMIT = NAPI.Resource.GetSetting<int>(this, "playerParkinglotLimit");
            if (NAPI.Resource.HasSetting(this, "parkinglotMoneyLimit")) PARKINGLOT_MONEY_LIMIT = NAPI.Resource.GetSetting<int>(this, "parkinglotMoneyLimit");
            if (NAPI.Resource.HasSetting(this, "saveInterval")) SAVE_INTERVAL = NAPI.Resource.GetSetting<int>(this, "saveInterval");

            NAPI.Util.ConsoleOutput("-> Player Parking lot Limit: {0}", ((PLAYER_PARKINGLOT_LIMIT == 0) ? "Disabled" : PLAYER_PARKINGLOT_LIMIT.ToString()));
            NAPI.Util.ConsoleOutput("-> Parking lot Safe Limit: ${0:n0}", PARKINGLOT_MONEY_LIMIT);

            // load parking lots
            foreach (string file in Directory.EnumerateFiles(PARKINGLOT_SAVE_DIR, "*.json"))
            {
                Parkinglot parkinglot = JsonConvert.DeserializeObject<Parkinglot>(File.ReadAllText(file));
                Parkinglots.Add(parkinglot);
            }

            NAPI.Util.ConsoleOutput("Loaded {0} parking lots.", Parkinglots.Count);
        }

        [ServerEvent(Event.ResourceStop)]
        public void OnResourceStop()
        {
            foreach (Parkinglot parkinglot in Parkinglots)
            {
                parkinglot.Save(true);
                parkinglot.Destroy();
            }

            Parkinglots.Clear();
        }
        #endregion
    }
}
