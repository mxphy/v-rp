﻿using System;
using System.Linq;
using gtalife.src.Player.Utils;
using gtalife.src.Database.Models;
using gtalife.src.Player.Inventory.Extensions;
using GTANetworkAPI;
using Newtonsoft.Json;
using gtalife.src.Player.Outfit;
using Microsoft.EntityFrameworkCore;

namespace gtalife.src.Player.Selection
{
    class CharacterSelection : Script
    {        
        [RemoteEvent("RetrieveCharactersList")]
        public void RetrieveCharactersList(Client player, object[] arguments)
        {
            if (!NAPI.Data.HasEntityData(player, "User")) return;

            using (var ctx = new DefaultDbContext())
            {
                var user = ctx.Users.FirstOrDefault(up => up.SocialClubName == player.SocialClubName);
                var characters = ctx.Characters.Include(c => c.Vehicles).Include(c => c.Contacts).Include(c => c.Clothes).Include(c => c.Trait).Include(c => c.Weapons).Where(up => up.User == user).ToList();
                dynamic jsonobject = JsonConvert.SerializeObject(characters, Formatting.Indented, new JsonSerializerSettings()
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                });
                NAPI.ClientEvent.TriggerClientEvent(player, "UpdateCharactersList", jsonobject);
            }
        }

        [RemoteEvent("SelectCharacter")]
        public void SelectCharacter(Client player, object[] arguments)
        {
            int characterId = Convert.ToInt32(arguments[0]);
            using (var ctx = new DefaultDbContext())
            {
                var character = ctx.Characters.Include(c => c.Vehicles).Include(c => c.Contacts).Include(c => c.Clothes).Include(c => c.Trait).Include(c => c.Weapons).FirstOrDefault(up => up.Id == characterId);

                player.Name = character.Name;
                player.loadInventory();
                player.setMoney(character.Money);

                if (character.WantedLevel > 0) player.WantedLevel = character.WantedLevel;

                NAPI.Entity.SetEntityPositionFrozen(player, false);
                NAPI.Entity.SetEntityPosition(player, new Vector3(character.PositionX, character.PositionY, character.PositionZ));
                NAPI.Entity.SetEntityRotation(player, new Vector3(0f, 0f, character.RotationZ));

                if (character.InsideHouseId != null) player.SetData("InsideHouse_ID", character.InsideHouseId);

                Managers.DimensionManager.DismissPrivateDimension(player);
                NAPI.Entity.SetEntityDimension(player, character.Dimension);

                player.SendChatMessage($"~g~* ¡Hola, {player.Name}, bienvenido de vuelta! - Última vez visto: {character.LastLogin.ToString("dd/MM/yyyy")}");

                Data.Character.Add(player, character);
                Data.LoadAccount(player);

                if (character.JailTime > 0) player.SetOutfit(67);

                // Send message to all if player is only taxi driver
                int taxiDrivers = 0;
                foreach (var p in NAPI.Pools.GetAllPlayers())
                {
                    if (p.GetJob() == (int)Job.JobType.Taxi)
                        taxiDrivers++;
                }

                if (taxiDrivers == 1)
                {
                    foreach (var p in NAPI.Pools.GetAllPlayers())
                    {
                        if (p == player) continue;
                        player.SendChatMessage("~y~INFO: ~w~Ya hay taxistas disponibles. (( /taxi ))");
                    }
                }

                // Sync player face with other players
                Customization.CustomizationModel gtao = new Customization.CustomizationModel();
                gtao.InitializePedFace(player);
                gtao.UpdatePlayerFace(player);
            }
        }

        [RemoteEvent("DeleteCharacter")]
        public void DeleteCharacter(Client player, object[] arguments)
        {
            int characterId = (int)arguments[0];
            using (var ctx = new DefaultDbContext())
            {
                var character = ctx.Characters.FirstOrDefault(up => up.Id == characterId);
                if (character != null)
                {
                    ctx.Characters.Remove(character);
                    ctx.SaveChanges();
                }

                if (!NAPI.Data.HasEntityData(player, "User")) return;
                User user = NAPI.Data.GetEntityData(player, "User");

                var characters = ctx.Characters.Where(up => up.User == user);
                NAPI.ClientEvent.TriggerClientEvent(player, "UpdateCharactersList", JsonConvert.SerializeObject(characters, Formatting.Indented, new JsonSerializerSettings { ReferenceLoopHandling = ReferenceLoopHandling.Ignore }));
            }
        }
    }
}
