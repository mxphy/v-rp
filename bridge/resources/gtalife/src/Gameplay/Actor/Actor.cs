﻿using GTANetworkAPI;
using Newtonsoft.Json;
using System;
using System.IO;

namespace gtalife.src.Gameplay.Actor
{
    #region Actor Class
    public class Actor
    {
        public Guid ID { get; }
        public string Name { get; private set; }
        public PedHash Skin { get; }
        public Vector3 Position { get; }
        public float Heading { get; }

        // anim
        public string AnimDict { get; private set; }
        public string AnimName { get; private set; }

        // entities
        [JsonIgnore]
        private Ped Ped;

        [JsonIgnore]
        private TextLabel Label;

        [JsonIgnore]
        private DateTime LastSave;

        public Actor(Guid id, string name, PedHash skin, Vector3 position, float heading, string animdict = "", string animname = "")
        {
            ID = id;
            Name = name;
            Skin = skin;
            Position = position;
            Heading = heading;

            // anim
            AnimDict = animdict;
            AnimName = animname;

            // create ped
            Ped = NAPI.Ped.CreatePed(Skin, Position, heading);

            // create label
            Label = NAPI.TextLabel.CreateTextLabel(Name, Position + new Vector3(0.0, 0.0, 1.25), 15f, 0.5f, 1, new Color(255, 255, 255, 255));

            // anim
            if (!string.IsNullOrEmpty(AnimDict) && !string.IsNullOrEmpty(AnimName)) Ped.PlayAnimation(AnimDict, AnimName, true);
        }

        public void SetName(string name)
        {
            Name = name;
            Label.Text = name;
        }

        public void PlayAnimation(string dict, string name)
        {
            AnimDict = dict;
            AnimName = name;

            Ped.PlayAnimation(AnimDict, AnimName, true);
        }

        public void StopAnimation()
        {
            AnimDict = "";
            AnimName = "";

            Ped.StopAnimation();
        }

        public void Save(bool force = false)
        {
            if (!force && DateTime.Now.Subtract(LastSave).TotalSeconds < Main.SAVE_INTERVAL) return;

            File.WriteAllText(Main.ACTOR_SAVE_DIR + Path.DirectorySeparatorChar + ID + ".json", JsonConvert.SerializeObject(this, Formatting.Indented));
            LastSave = DateTime.Now;
        }

        public void Destroy()
        {
            Label.Delete();
            Ped.Delete();
        }
    }
    #endregion
}
