const NativeUI = require("nativeui");
const Menu = NativeUI.Menu;
const UIMenuItem = NativeUI.UIMenuItem;
const Point = NativeUI.Point;
const Offset = require("gtalife/util/offsets.js")

mp.events.add('ShowInteriorBrowser', (category, interior) => {
    var interiors = JSON.parse(interior);
    var categories = JSON.parse(category);

    let menu = new Menu("Interiors", "Seleciona una categoría", new Point(mp.game.resolution.width - Offset.getWidthOffset(mp.game.resolution.width), 50));
    for (let i = 0; i < categories.length; i++) {
        menu.AddItem(new UIMenuItem(categories[i], ""));
    }

    menu.Visible = true;
    menu.ItemSelect.on((item, index) => {
        menu.Visible = false;
        let submenu = new Menu(categories[index], "Seleciona un interior", new Point(mp.game.resolution.width - Offset.getWidthOffset(mp.game.resolution.width), 50));
        for (let i = 0; i < interiors.length; i++) {
            if (interiors[i].Category == index) {
                submenu.AddItem(new UIMenuItem(interiors[i].Name, ""));
            }
        }

        submenu.Visible = true;
        submenu.ItemSelect.on((subitem) => {
            submenu.Visible = false;
            mp.events.callRemote('TeleportToInterior', interiors.findIndex(x => x.Name == subitem.Text));
        });
    });
});
