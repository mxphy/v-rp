﻿using System;
using System.IO;
using System.Linq;
using GTANetworkAPI;
using gtalife.src.Admin;
using gtalife.src.Managers;
using gtalife.src.Player.Utils;

namespace gtalife.src.Faction
{
    class Commands : Script
    {
        #region AdminCommands
        [Command("factioncmds")]
        public void AdminCommands(Client player)
        {
            player.SendChatMessage("!{#a5f413}~~~~~~~~~~~ Comandos Admin ~~~~~~~~~~~");
            player.SendChatMessage("* /cfaction - /sfname - /sftype - /rfaction - /spfaction - /rpfaction - /cfrank - /sfrank - /spfrank");
            player.SendChatMessage("* /flist - /franks - /ftypes");
            player.SendChatMessage("!{#a5f413}~~~~~~~~~~~ Comandos Admin ~~~~~~~~~~~");
        }

        [Command("cfaction", GreedyArg = true)]
        public void CMD_CreateFaction(Client player, int type, string name)
        {
            if (player.GetAdminLevel() < 4) player.SendChatMessage("~r~ERROR: ~w~No tienes permison.");
            else if (type < 0 || type > Enum.GetNames(typeof(FactionType)).Length) player.SendChatMessage("~r~ERROR: ~w~Tipo inválido.");
            else if (Main.Factions.Count(f => f.Name == name) > 0) player.SendChatMessage("~r~ERROR: ~w~Este nombre ya está usado.");
            else
            {
                Faction new_faction = new Faction(Main.GetGuid(), name, type);
                new_faction.Save();

                Main.Factions.Add(new_faction);

                FactionType factionType = (FactionType)type;
                player.SendChatMessage($"~g~ÉXITO: ~w~Creaste una facción llamada ~g~{name}~s~ del tipo ~g~{factionType.ToString()}~s~.");
            }
        }

        [Command("sfname", GreedyArg = true)]
        public void CMD_FactionName(Client player, string name, string new_name)
        {
            if (player.GetAdminLevel() < 4) player.SendChatMessage("~r~ERROR: ~w~No tienes permison.");
            else
            {
                Faction faction = Main.Factions.FirstOrDefault(f => f.Name == name);
                if (faction == null) player.SendChatMessage("~r~ERROR: ~w~No se ha encontrado ninguna facción con este nombre.");
                else
                {
                    player.SendChatMessage(string.Format("~g~ÉXITO: ~w~Nombre de la facción ~g~{0} ~w~ajustado a ~g~\"{1}\"~w~.", faction.Name, new_name));

                    faction.Name = new_name;
                    faction.Save(true);
                }
            }
        }

        [Command("sftype")]
        public void CMD_FactionType(Client player, string name, int new_type)
        {
            if (player.GetAdminLevel() < 4) player.SendChatMessage("~r~ERROR: ~w~No tienes permison.");
            else if (new_type < 0 || new_type > Enum.GetNames(typeof(FactionType)).Length) player.SendChatMessage("~r~ERROR: ~w~ID del tipo es inválido.");
            else
            {
                Faction faction = Main.Factions.FirstOrDefault(f => f.Name == name);
                if (faction == null) player.SendChatMessage("~r~ERROR: ~w~No se ha encontrado ninguna facción con este nombre.");
                else
                {
                    FactionType factionType = (FactionType)new_type;
                    player.SendChatMessage(string.Format("~g~ÉXITO: ~w~Tipo de la facción ~g~{0} ~w~ajustado a ~g~\"{1}\"~w~.", faction.Name, factionType.ToString()));

                    faction.Type = new_type;
                    faction.Save(true);
                }
            }
        }

        [Command("rfaction", GreedyArg = true)]
        public void CMD_RemoveFaction(Client player, string name)
        {
            if (player.GetAdminLevel() < 4) player.SendChatMessage("~r~ERROR: ~w~No tienes permison.");
            else
            {
                Faction faction = Main.Factions.FirstOrDefault(f => f.Name == name);
                if (faction == null) player.SendChatMessage("~r~ERROR: ~w~No se ha encontrado ninguna facción con este nombre.");
                else
                {
                    Main.Factions.Remove(faction);

                    string faction_file = Main.FACTION_SAVE_DIR + Path.DirectorySeparatorChar + faction.ID + ".json";
                    if (File.Exists(faction_file)) File.Delete(faction_file);
                }
            }
        }

        [Command("spfaction", GreedyArg = true)]
        public void CMD_SetPlayerFaction(Client player, string idOrName, string factionName)
        {
            if (player.GetAdminLevel() < 4) player.SendChatMessage("~r~ERROR: ~w~No tienes permison.");
            else
            {
                var target = PlayeridManager.FindPlayer(idOrName);
                Faction faction = Main.Factions.FirstOrDefault(f => f.Name == factionName);

                if (target == null) player.SendChatMessage("~r~ERROR: ~w~Demasiados o ningún jugador encontrado.");
                else if (!target.IsLogged()) player.SendChatMessage("~r~ERROR: ~w~El jugador no está conectado.");
                else if (faction == null) player.SendChatMessage("~r~ERROR: ~w~No se ha encontrado ninguna facción con este nombre.");
                else if (faction.Ranks.Count < 2) player.SendChatMessage("~r~ERROR: ~w~Establece los rangos de la facción primero. (( /cfrank ))");
                else
                {
                    if (player != target) target.SendChatMessage($"~y~ADMIN: ~w~{player.Name} cambió tu facción a {factionName}.");
                    player.SendChatMessage($"~y~ADMIN: ~w~Has cambiado la facción de {target.Name} a {factionName}.");

                    target.SetFactionID(faction.ID);
                    target.SetFactionRank(faction.Ranks.Count - 1);
                }
            }
        }

        [Command("rpfaction")]
        public void CMD_RemovePlayerFaction(Client player, string idOrName)
        {
            if (player.GetAdminLevel() < 4) player.SendChatMessage("~r~ERROR: ~w~No tienes permison.");
            else
            {
                var target = PlayeridManager.FindPlayer(idOrName);
                if (target == null) player.SendChatMessage("~r~ERROR: ~w~Demasiados o ningún jugador encontrado.");
                else if (!target.IsLogged()) player.SendChatMessage("~r~ERROR: ~w~El jugador no está conectado.");
                else
                {
                    if (player != target) target.SendChatMessage($"~y~ADMIN: ~w~{player.Name} te sacó de tu facción.");
                    player.SendChatMessage($"~y~ADMIN: ~w~Sacaste a {target.Name} de su facción.");

                    target.SetFactionID(null);
                    target.SetFactionRank(null);
                }
            }
        }

        [Command("sfrank", GreedyArg = true)]
        public void CMD_SetFactionRank(Client player, string factionName, int rankId, string rankName)
        {
            if (player.GetAdminLevel() < 4) player.SendChatMessage("~r~ERROR: ~w~No tienes permison.");
            else
            {
                Faction faction = Main.Factions.FirstOrDefault(f => f.Name == factionName);

                if (faction == null) player.SendChatMessage("~r~ERROR: ~w~No se ha encontrado ninguna facción con este nombre.");
                else if (rankId < 0 || rankId > faction.Ranks.Count - 1) player.SendChatMessage("~r~ERROR: ~w~Rango inválido.");
                else
                {
                    player.SendChatMessage($"~y~ADMIN: ~w~Has cambiado lo nombre del rango ~g~{rankId} ~w~de ~g~{factionName} ~w~a ~g~{rankName}~w~.");

                    faction.Ranks[rankId] = rankName;
                    faction.Save(true);
                }
            }
        }

        [Command("cfrank", GreedyArg = true)]
        public void CMD_CreateFactionRank(Client player, string factionName, string rankName)
        {
            if (player.GetAdminLevel() < 4) player.SendChatMessage("~r~ERROR: ~w~No tienes permison.");
            else
            {
                Faction faction = Main.Factions.FirstOrDefault(f => f.Name == factionName);

                if (faction == null) player.SendChatMessage("~r~ERROR: ~w~No se ha encontrado ninguna facción con este nombre.");
                else
                {
                    faction.Ranks.Add(rankName);
                    faction.Save(true);

                    player.SendChatMessage($"~y~ADMIN: ~w~Has creado lo rango ~g~{faction.Ranks.Count - 1} ~w~de ~g~{factionName} ~w~a ~g~{rankName}~w~.");
                }
            }
        }

        [Command("spfrank")]
        public void CMD_SetPlayerFactionRank(Client player, string idOrName, int rank)
        {
            if (player.GetAdminLevel() < 4) player.SendChatMessage("~r~ERROR: ~w~No tienes permison.");
            else
            {
                var target = PlayeridManager.FindPlayer(idOrName);
                Faction faction = Main.Factions.FirstOrDefault(f => f.ID == target.GetFactionID());

                if (target == null) player.SendChatMessage("~r~ERROR: ~w~Demasiados o ningún jugador encontrado.");
                else if (!target.IsLogged()) player.SendChatMessage("~r~ERROR: ~w~El jugador no está conectado.");
                else if (faction == null) player.SendChatMessage("~r~ERROR: ~w~No se ha encontrado la facción del jugador.");
                else if (rank < 0 || rank > faction.Ranks.Count - 1) player.SendChatMessage("~r~ERROR: ~w~Rango inválido.");
                else
                {
                    if (player != target) target.SendChatMessage($"~y~ADMIN: ~w~{player.Name} cambió tu rango a {faction.Ranks[rank]}.");
                    player.SendChatMessage($"~y~ADMIN: ~w~Has cambiado lo rango de {target.Name} a {faction.Ranks[rank]}.");

                    target.SetFactionRank(rank);
                }
            }
        }

        [Command("flist")]
        public void CMD_FactionList(Client player)
        {
            if (player.GetAdminLevel() < 4) player.SendChatMessage("~r~ERROR: ~w~No tienes permison.");
            else
            {
                foreach (var faction in Main.Factions)
                {
                    FactionType factionType = (FactionType)faction.Type;
                    player.SendChatMessage($"Faction name: ~g~{faction.Name} ~w~- Faction type: ~g~{factionType.ToString()}");
                }
            }
        }

        [Command("franks", GreedyArg = true)]
        public void CMD_FactionRanks(Client player, string factionName)
        {
            if (player.GetAdminLevel() < 4) player.SendChatMessage("~r~ERROR: ~w~No tienes permison.");
            else
            {
                foreach (var faction in Main.Factions)
                {
                    foreach (var rank in faction.Ranks)
                    {
                        player.SendChatMessage($"Rank name: ~g~{rank} ~w~- Rank id: ~g~{faction.Ranks.IndexOf(rank)}");
                    }
                }
            }
        }

        [Command("ftypes")]
        public void CMD_FactionTypes(Client player)
        {
            if (player.GetAdminLevel() < 4) player.SendChatMessage("~r~ERROR: ~w~No tienes permison.");
            else
            {
                foreach (var type in Enum.GetValues(typeof(FactionType)))
                {
                    player.SendChatMessage($"Type id: ~g~{(int)type} ~w~- Type name: ~g~{type.ToString()}");
                }
            }
        }
        #endregion

        #region LeaderCommands
        [Command("promover")]
        public void CMD_PromotePlayer(Client player, string idOrName)
        {
            var target = PlayeridManager.FindPlayer(idOrName);
            Faction faction = Main.Factions.FirstOrDefault(f => f.ID == player.GetFactionID());

            if (target == null) player.SendChatMessage("~r~ERROR: ~w~Demasiados o ningún jugador encontrado.");
            else if (!target.IsLogged()) player.SendChatMessage("~r~ERROR: ~w~El jugador no está conectado.");
            else if (faction == null || player.GetFactionRank() > 0) player.SendChatMessage("~r~ERROR: ~w~No tienes permiso.");
            else if (!target.IsLogged()) player.SendChatMessage("~r~ERROR: ~w~Este jugador no esta conectado.");
            else if (target.GetFactionID() != player.GetFactionID()) player.SendChatMessage("~r~ERROR: ~w~Este jugador no es de su facción.");
            else if (target.GetFactionRank() < 2) player.SendChatMessage("~r~ERROR: ~w~Ya no puedes promocionar a este jugador.");
            else
            {
                Player.Data.Character[target].FacRank--;

                player.SendChatMessage($"~g~ÉXITO: ~w~Usted promovió {target.Name} a {faction.Ranks[(int)Player.Data.Character[player].FacRank]}.");
                target.SendChatMessage($"~g~ÉXITO: ~w~{player.Name} promovió usted a {faction.Ranks[(int)Player.Data.Character[player].FacRank]}.");
            }
        }

        [Command("degradar")]
        public void CMD_DemotePlayer(Client player, string idOrName)
        {
            var target = PlayeridManager.FindPlayer(idOrName);
            Faction faction = Main.Factions.FirstOrDefault(f => f.ID == player.GetFactionID());

            if (target == null) player.SendChatMessage("~r~ERROR: ~w~Demasiados o ningún jugador encontrado.");
            else if (!target.IsLogged()) player.SendChatMessage("~r~ERROR: ~w~El jugador no está conectado.");
            else if (faction == null || player.GetFactionRank() > 0) player.SendChatMessage("~r~ERROR: ~w~No tienes permiso.");
            else if (player.GetFactionRank() > 0) player.SendChatMessage("~r~ERROR: ~w~No tienes permiso.");
            else if (!target.IsLogged()) player.SendChatMessage("~r~ERROR: ~w~Este jugador no esta conectado.");
            else if (target.GetFactionID() != player.GetFactionID()) player.SendChatMessage("~r~ERROR: ~w~Este jugador no es de su facción.");
            else if (target.GetFactionRank() >= faction.Ranks.Count) player.SendChatMessage("~r~ERROR: ~w~Ya no puedes degradar a este jugador más.");
            else
            {
                Player.Data.Character[target].FacRank++;

                player.SendChatMessage($"~g~ÉXITO: ~w~Usted degradó {target.Name} a {faction.Ranks[(int)Player.Data.Character[player].FacRank]}.");
                target.SendChatMessage($"~y~INFO: ~w~{player.Name} degradó usted a {faction.Ranks[(int)Player.Data.Character[player].FacRank]}.");
            }
        }

        [Command("fdespedir")]
        public void CMD_FirePlayer(Client player, string idOrName)
        {
            var target = PlayeridManager.FindPlayer(idOrName);
            Faction faction = Main.Factions.FirstOrDefault(f => f.ID == player.GetFactionID());

            if (target == null) player.SendChatMessage("~r~ERROR: ~w~Demasiados o ningún jugador encontrado.");
            else if (!target.IsLogged()) player.SendChatMessage("~r~ERROR: ~w~El jugador no está conectado.");
            else if (faction == null || player.GetFactionRank() > 0) player.SendChatMessage("~r~ERROR: ~w~No tienes permiso.");
            else if (player.GetFactionRank() > 0) player.SendChatMessage("~r~ERROR: ~w~No tienes permiso.");
            else if (!target.IsLogged()) player.SendChatMessage("~r~ERROR: ~w~Este jugador no esta conectado.");
            else if (target.GetFactionID() != player.GetFactionID()) player.SendChatMessage("~r~ERROR: ~w~Este jugador no es de su facción.");
            else
            {
                Player.Data.Character[target].Faction = null;
                Player.Data.Character[target].FacRank = null;

                player.SendChatMessage($"~g~ÉXITO: ~w~Usted despedió {target.Name}.");
                target.SendChatMessage($"~y~INFO: ~w~{player.Name} despedió usted.");

                if (NAPI.Data.HasEntityData(target, "IsUsingPoliceUniform")) Police.Police.TogglePlayerOnDuty(target);
            }
        }

        [Command("invitar")]
        public void CMD_InvitePlayer(Client player, string idOrName)
        {
            var target = PlayeridManager.FindPlayer(idOrName);
            Faction faction = Main.Factions.FirstOrDefault(f => f.ID == player.GetFactionID());

            if (target == null) player.SendChatMessage("~r~ERROR: ~w~Demasiados o ningún jugador encontrado.");
            else if (!target.IsLogged()) player.SendChatMessage("~r~ERROR: ~w~El jugador no está conectado.");
            else if (faction == null || player.GetFactionRank() > 0) player.SendChatMessage("~r~ERROR: ~w~No tienes permiso.");
            else if (player.GetFactionRank() > 0) player.SendChatMessage("~r~ERROR: ~w~No tienes permiso.");
            else if (!target.IsLogged()) player.SendChatMessage("~r~ERROR: ~w~Este jugador no esta conectado.");
            else if (target.GetFactionID() != null) player.SendChatMessage("~r~ERROR: ~w~Este jugador ya tienes facción.");
            else
            {
                player.SendChatMessage($"~y~INFO: ~w~Usted invitó {target.Name} para la {faction.Name}.");
                target.SendChatMessage($"~y~INFO: ~w~{player.Name} invitó usted para la {faction.Name}. (( /aceptarfaccion ))");

                target.SetData("FACTION_INVITE_PLAYER", player);
            }
        }

        [Command("aceptarfaccion")]
        public void CMD_AcceptFacction(Client player)
        {
            if (!player.HasData("FACTION_INVITE_PLAYER")) player.SendChatMessage("~r~ERROR: ~w~Ningun ha te invitado.");
            else
            {
                Client leader = player.GetData("FACTION_INVITE_PLAYER");
                player.ResetData("FACTION_INVITE_PLAYER");

                if (!leader.IsLogged()) player.SendChatMessage("~r~ERROR: ~w~El jugador no estás más online.");
                else
                {
                    leader.SendChatMessage($"~g~ÉXITO: ~w~{player.Name} ha aceptado el invite.");
                    player.SendChatMessage($"~g~ÉXITO: ~w~Usted ha aceptado el invite.");

                    Faction faction = Main.Factions.FirstOrDefault(f => f.ID == Player.Data.Character[leader].Faction);

                    Player.Data.Character[player].Faction = faction.ID;
                    Player.Data.Character[player].FacRank = faction.Ranks.Count - 1;
                }
            }
        }

        [Command("cfvehiculo", "~y~USO: ~w~/cfvehiculo [modelo]")]
        public void CMD_CreateFactionVehicle(Client player, VehicleHash model)
        {
            Faction faction = Main.Factions.FirstOrDefault(f => f.ID == player.GetFactionID());
            if (player.GetAdminLevel() < 4 && (faction == null || player.GetFactionRank() > 0)) player.SendChatMessage("~r~ERROR: ~w~No tienes permiso.");
            else
            {
                FactionVehicle new_vehicle = new FactionVehicle((int)model, 0, 111, player.Position, new Vector3());
                faction.Vehicles.Add(new_vehicle);

                new_vehicle.Create();
                faction.Save();

                player.SendChatMessage($"~g~EXITO: ~s~Creaste un {NAPI.Vehicle.GetVehicleDisplayName(model)}~s~.");
            }
        }

        [Command("rfvehiculo")]
        public void CMD_RemoveFactionVehicle(Client player)
        {
            Faction faction = Main.Factions.FirstOrDefault(f => f.ID == player.GetFactionID());
            if (player.GetAdminLevel() < 4 && (faction == null || player.GetFactionRank() > 0)) player.SendChatMessage("~r~ERROR: ~w~No tienes permiso.");
            else if (!player.IsInVehicle) player.SendChatMessage("~r~ERROR: ~w~No estas en un vehículo.");
            else
            {
                foreach (FactionVehicle vehicle in faction.Vehicles)
                {
                    if (vehicle.Vehicle == player.Vehicle)
                    {
                        vehicle.Destroy();
                        faction.Vehicles.Remove(vehicle);
                        faction.Save(true);

                        player.SendChatMessage($"~g~EXITO: ~s~Borraste lo vehículo.");
                        return;
                    }
                }
                player.SendChatMessage("~r~ERROR: ~w~No estas en un vehículo de la SASD~s~.");
            }
        }

        [Command("fsvehiculopos")]
        public void CMD_SetFactionVehiclePos(Client player)
        {
            Faction faction = Main.Factions.FirstOrDefault(f => f.ID == player.GetFactionID());
            if (player.GetAdminLevel() < 4 && (faction == null || player.GetFactionRank() > 0)) player.SendChatMessage("~r~ERROR: ~w~No tienes permiso.");
            else if (!player.IsInVehicle) player.SendChatMessage("~r~ERROR: ~w~No estas en un vehículo.");
            else
            {
                foreach (FactionVehicle vehicle in faction.Vehicles)
                {
                    if (vehicle.Vehicle == player.Vehicle)
                    {
                        vehicle.Position = player.Vehicle.Position;
                        vehicle.Rotation = player.Vehicle.Rotation;
                        faction.Save(true);

                        player.SendChatMessage($"~g~EXITO: ~s~Cambiaste la posicion de lo vehículo~s~.");
                        return;
                    }
                }
                player.SendChatMessage("~r~ERROR: ~w~No estas en un vehículo de un facción.");
            }
        }        
        #endregion
    }
}
