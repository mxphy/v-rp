﻿using System.IO;
using System.Linq;
using GTANetworkAPI;
using gtalife.src.Database.Models;

namespace gtalife.src.Gameplay.Business
{
    class Commands : Script
    {
        [Command("crearnegocio")]
        public void CMD_CreateBusiness(Client player, int type, int price)
        {
            User user = NAPI.Data.GetEntityData(player, "User");
            if (user.Admin < 4)
            {
                player.SendChatMessage("~r~ERROR: ~w~No tienes permison.");
                return;
            }

            if (type < 0 || type >= BusinessTypes.BusinessTypeList.Count)
            {
                player.SendChatMessage("~r~ERROR: ~w~ID del tipo es inválido.");
                return;
            }

            Business new_business = new Business(Main.GetGuid(), string.Empty, type, player.Position, price, false, player.Dimension);
            new_business.Save();

            Main.Businesses.Add(new_business);
        }

        [Command("setnegocionombre", GreedyArg = true)]
        public void CMD_BusinessName(Client player, string new_name)
        {
            User user = NAPI.Data.GetEntityData(player, "User");
            if (user.Admin < 4)
            {
                player.SendChatMessage("~r~ERROR: ~w~No tienes permison.");
                return;
            }

            if (!player.HasData("BusinessMarker_ID"))
            {
                player.SendChatMessage("~r~ERROR: ~w~Párese en el marcador de entrada de lo negocio que desea editar.");
                return;
            }

            Business business = Main.Businesses.FirstOrDefault(h => h.ID == player.GetData("BusinessMarker_ID"));
            if (business == null) return;

            business.SetName(new_name);
            player.SendChatMessage(string.Format("~b~NEGOCIO: ~w~Nombre ajustado para ~b~\"{0}\".", new_name));
        }

        [Command("setnegociotipo")]
        public void CMD_BusinessType(Client player, int new_type)
        {
            User user = NAPI.Data.GetEntityData(player, "User");
            if (user.Admin < 4)
            {
                player.SendChatMessage("~r~ERROR: ~w~No tienes permison.");
                return;
            }

            if (!player.HasData("BusinessMarker_ID"))
            {
                player.SendChatMessage("~r~ERROR: ~w~Párese en el marcador de entrada de lo negocio que desea editar.");
                return;
            }

            if (new_type < 0 || new_type >= BusinessTypes.BusinessTypeList.Count)
            {
                player.SendChatMessage("~r~ERROR: ~w~ID del tipo es inválido.");
                return;
            }

            Business business = Main.Businesses.FirstOrDefault(h => h.ID == player.GetData("BusinessMarker_ID"));
            if (business == null) return;

            business.SetType(new_type);
            player.SendChatMessage(string.Format("~b~NEGOCIO: ~w~Tipo ajustado para ~b~{0}.", BusinessTypes.BusinessTypeList[new_type].Name));
        }

        [Command("setnegocioprecio")]
        public void CMD_BusinessPrice(Client player, int new_price)
        {
            User user = NAPI.Data.GetEntityData(player, "User");
            if (user.Admin < 4)
            {
                player.SendChatMessage("~r~ERROR: ~w~No tienes permison.");
                return;
            }

            if (!player.HasData("BusinessMarker_ID"))
            {
                player.SendChatMessage("~r~ERROR: ~w~Párese en el marcador de entrada de lo negocio que desea editar.");
                return;
            }

            Business business = Main.Businesses.FirstOrDefault(h => h.ID == player.GetData("BusinessMarker_ID"));
            if (business == null) return;

            business.SetPrice(new_price);
            player.SendChatMessage(string.Format("~b~NEGOCIO: ~w~Precio ajustado para ~g~${0:n0}.", new_price));
        }

        [Command("borrarnegocio")]
        public void CMD_RemoveBusiness(Client player)
        {
            User user = NAPI.Data.GetEntityData(player, "User");
            if (user.Admin < 4)
            {
                player.SendChatMessage("~r~ERROR: ~w~No tienes permison.");
                return;
            }

            if (!player.HasData("BusinessMarker_ID"))
            {
                player.SendChatMessage("~r~ERROR: ~w~Párese en el marcador de entrada de la negocio que desea borrar.");
                return;
            }

            Business business = Main.Businesses.FirstOrDefault(h => h.ID == player.GetData("BusinessMarker_ID"));
            if (business == null) return;

            business.Destroy();
            Main.Businesses.Remove(business);

            string business_file = Main.BUSINESS_SAVE_DIR + Path.DirectorySeparatorChar + business.ID + ".json";
            if (File.Exists(business_file)) File.Delete(business_file);
        }

        [Command("añadirnegocioveh", "~y~USO: ~w~/añadirnegocioveh [precio]", Alias = "addbusinessveh")]
        public void CMD_AddDealershipVehicle(Client sender, int price)
        {
            if (!sender.IsInVehicle)
                NAPI.Chat.SendChatMessageToPlayer(sender, $"~r~ERROR: ~s~Necesitas estar en un vehículo.");
            else if (price < 1)
                NAPI.Chat.SendChatMessageToPlayer(sender, $"~r~ERROR: ~s~Precio invalido.");
            else
            {
                foreach (Business business in Main.Businesses)
                {
                    if (business.Position.DistanceTo(sender.Position) < 40f)
                    {
                        if (business.VehicleSpawn.X == 0.0f && business.VehicleSpawn.Y == 0.0f)
                        {
                            NAPI.Chat.SendChatMessageToPlayer(sender, "~r~ERROR: ~w~Primero debe configurar el spawn del vehículo. (/negociovspawn)");
                            return;
                        }

                        BusinessVehicle new_vehicle = new BusinessVehicle(sender.Vehicle.Model, sender.Vehicle.PrimaryColor, sender.Vehicle.SecondaryColor, sender.Vehicle.Position, sender.Vehicle.Rotation, price);
                        business.Vehicles.Add(new_vehicle);

                        sender.Vehicle.Delete();
                        sender.Position.Z += 0.5f;

                        new_vehicle.Create();
                        business.Save();

                        if (string.IsNullOrEmpty(business.Name))
                            NAPI.Chat.SendChatMessageToPlayer(sender, $"~g~EXITO: ~s~Creaste un {sender.Vehicle.DisplayName} para lo negocio.");
                        else
                            NAPI.Chat.SendChatMessageToPlayer(sender, $"~g~EXITO: ~s~Creaste un {sender.Vehicle.DisplayName} para lo negocio ~y~{business.Name}~s~.");
                        return;
                    }
                }

                NAPI.Chat.SendChatMessageToPlayer(sender, $"~r~ERROR: ~s~No estás cerca de un negocio.");
            }
        }

        [Command("borrarnegocioveh")]
        public void CMD_RemoveDealershipVehicle(Client sender)
        {
            if (!sender.IsInVehicle)
                NAPI.Chat.SendChatMessageToPlayer(sender, $"~r~ERROR: ~s~Necesitas estar en un vehículo.");
            else
            {
                foreach (Business business in Main.Businesses)
                {
                    foreach (BusinessVehicle vehicle in business.Vehicles)
                    {
                        if (sender.Vehicle == vehicle.Vehicle)
                        {
                            vehicle.Destroy();
                            business.Vehicles.Remove(vehicle);
                            business.Save();

                            if (string.IsNullOrEmpty(business.Name))
                                NAPI.Chat.SendChatMessageToPlayer(sender, $"~g~EXITO: ~s~Borraste o vehiculo de lo negocio.");
                            else
                                NAPI.Chat.SendChatMessageToPlayer(sender, $"~g~EXITO: ~s~Borraste o vehiculo de lo negocio ~y~{business.Name}~s~.");
                            return;
                        }
                    }
                }

                NAPI.Chat.SendChatMessageToPlayer(sender, $"~r~ERROR: ~s~No estás cerca de un negocio.");
            }
        }

        [Command("negociovspawn")]
        public void CMD_DealershipVehicleSpawn(Client sender)
        {
            if (!sender.IsInVehicle)
                NAPI.Chat.SendChatMessageToPlayer(sender, $"~r~ERROR: ~s~Necesitas estar en un vehículo.");
            else
            {
                var distance = 40f;
                foreach (Business business in Main.Businesses)
                {
                    if (business.Position.DistanceTo(sender.Position) < distance)
                    {
                        distance = business.Position.DistanceTo(sender.Position);
                        business.SetSpawn(sender.Vehicle.Position);

                        if (string.IsNullOrEmpty(business.Name))
                            NAPI.Chat.SendChatMessageToPlayer(sender, $"~g~EXITO: ~s~Usted has configurado el spawn para los vehiculos del negocio.");
                        else
                            NAPI.Chat.SendChatMessageToPlayer(sender, $"~g~EXITO: ~s~Usted has configurado el spawn para los vehiculos de ~y~{business.Name}~s~.");
                        return;
                    }
                }

                NAPI.Chat.SendChatMessageToPlayer(sender, $"~r~ERROR: ~s~No estás cerca de un negocio.");
            }
        }

        [Command("negociocmds")]
        public void AdminCommands(Client player)
        {
            NAPI.Chat.SendChatMessageToPlayer(player, "!{#a5f413}~~~~~~~~~~~ Comandos Admin ~~~~~~~~~~~");
            NAPI.Chat.SendChatMessageToPlayer(player, "* /crearnegocio - /setnegocionombre - /setnegociotipo - /setnegocioprecio - /borrarnegocio");
            NAPI.Chat.SendChatMessageToPlayer(player, "* /negociovspawn - /añadirnegocioveh - /borrarnegocioveh");
            NAPI.Chat.SendChatMessageToPlayer(player, "!{#a5f413}~~~~~~~~~~~ Comandos Admin ~~~~~~~~~~~");
        }
    }
}
