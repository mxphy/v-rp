﻿/// <reference path='../../../types-gt-mp/index.d.ts' />

var menu_list = null;
var weapon_menus = [];
var police_text_mode = 0;

var menu_weapons = API.createMenu("Armas", "Seleciona una arma", 0, 0, 6);
API.setMenuBannerSprite(menu_weapons, "shopui_title_gr_gunmod", "shopui_title_gr_gunmod");
weapon_menus.push(menu_weapons);

var wpClassNames = [
    "Pistolas",
    "Ametralladoras",
    "Rifles de asalto",
    "Francotiradores",
    "Escopetas",
    "Cuerpo a cuerpo"
];

var wpHashes = [
    { class: 0, hash: 911657153 }, // StunGun
    { class: 0, hash: 453432689 }, // Pistol
    { class: 0, hash: -1716589765 }, // Pistol50
    { class: 0, hash: -1045183535 }, // Revolver
    { class: 1, hash: 324215364 }, // MicroSMG
    { class: 1, hash: 736523883 }, // SMG
    { class: 1, hash: 1627465347 }, // Gusenberg
    { class: 2, hash: -1074790547 }, // AssaltRifle
    { class: 2, hash: 2132975508 },// BullpupRifle
    { class: 3, hash: 100416529 },// SniperRifle
    { class: 4, hash: 487013001 },// PumpShotgun
    { class: 4, hash: 2017895192 },// SawnOffShotgun
    { class: 5, hash: 1737195953 },// Nightstick
    { class: 5, hash: -1951375401 }// Flashlight
];

API.onResourceStart.connect(() => {
    var groupedWeapons = [];

    for (var i = 0; i < wpHashes.length; i++) {
        var weaponHash = wpHashes[i].hash;
        var weaponClass = wpClassNames[wpHashes[i].class];

        if (groupedWeapons[weaponClass] == undefined) {
            groupedWeapons[weaponClass] = [];
        }

        groupedWeapons[weaponClass].push({ hash: weaponHash, name: API.getWeaponName(weaponHash) });
    }

    for (var group in groupedWeapons) {
        if (!groupedWeapons.hasOwnProperty(group)) continue;

        var groupName = group;
        var weapons = groupedWeapons[group];
        var categoryMenu = createPoliceWeaponCategory(groupName);

        for (var i = 0; i < weapons.length; i++) {
            var weapon = weapons[i];
            createPoliceSpawnWeaponItem(weapon.name, weapon.hash, categoryMenu);
        }
    }
});

API.onServerEventTrigger.connect((eventName: string, args: System.Array<any>) => {
    switch (eventName) {
        case "Police_ShowMenuList":
            var data = JSON.parse(args[0]);
            if (menu_list == null) {
                menu_list = API.createMenu(" ", " ", 0, 0, 6);
            }
            menu_list.Clear();
            API.setMenuTitle(menu_list, args[1]);

            for (let i = 0; i < data.length; i++) {
                var temp_item = API.createMenuItem(data[i], "");
                menu_list.AddItem(temp_item);
            }
            menu_list.Visible = true;
            break;
        case "Police_ShowWeaponMenu":
            menu_weapons.Visible = true;
            break;
        case "OnEnterPoliceMark":
            police_text_mode = args[0];
            break;
        case "OnLeavePoliceMark":
            police_text_mode = 0;
            break;
    }
});

API.onKeyDown.connect((sender: any, e: System.Windows.Forms.KeyEventArgs) => {
    if (police_text_mode == 0) return;

    if (e.KeyCode === Keys.E) {
        API.triggerServerEvent("PoliceInteract");
    }
});

API.onUpdate.connect(() => {
    if (police_text_mode > 0) API.displaySubtitle("Pulsa ~y~E ~w~para interactuar.", 100);
});

function createPoliceWeaponCategory(name) {
    var weaponCategoryMenu = API.createMenu("Armas", 0, 0, 6);
    API.setMenuBannerSprite(weaponCategoryMenu, "shopui_title_gr_gunmod", "shopui_title_gr_gunmod");
    weapon_menus.push(weaponCategoryMenu);

    var weaponCategoryItem = API.createMenuItem(name, "");

    menu_weapons.AddItem(weaponCategoryItem);
    menu_weapons.BindMenuToItem(weaponCategoryMenu, weaponCategoryItem);

    return weaponCategoryMenu;
}

function createPoliceSpawnWeaponItem(name, hash, parentMenu) {
    var menuItem = API.createMenuItem(name, "");
    menuItem.Activated.connect(function (menu, item) {
        for (var i = 0; i < weapon_menus.length; i++) {
            if (weapon_menus[i].Visible) {
                weapon_menus[i].Visible = false;
            }
        }
        API.triggerServerEvent("POLICE_GET_WEAPON", hash);
    });
    parentMenu.AddItem(menuItem);
}
