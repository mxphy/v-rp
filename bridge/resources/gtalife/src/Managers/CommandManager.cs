﻿using GTANetworkAPI;
using System.Collections.Generic;

namespace gtalife.src.Managers
{
    public static class CommandManager
    {
        public static Client FindPlayer(Client sender, string playerName)
        {
            playerName = playerName.ToLower();
            List<Client> players = NAPI.Pools.GetAllPlayers();
            var target = players.Find(x => x.Name.ToLower().Equals(playerName));
            if (target != null)
            {
                return target;
            }
            else
            {
                var targetList = players.FindAll(x => x.Name.ToLower().Contains(playerName));
                if (targetList.Count < 1)
                {
                    NAPI.Notification.SendNotificationToPlayer(sender, "~r~ERRO: ~w~Nenhum jugador com este nombre encontrado.");
                    return null;
                }
                else if (targetList.Count > 1)
                {
                    NAPI.Notification.SendNotificationToPlayer(sender, "~r~ERRO: ~w~Mas de uno jugador encontrado, especifique mejor.");
                    return null;
                }
                else
                {
                    return targetList[0];
                }
            }
        }
    }
}
