﻿using GTANetworkAPI;
using gtalife.src.Managers;

namespace gtalife.src.Gameplay.Doors
{
    class Doors : Script
    {

        [ServerEvent(Event.ResourceStart)]
        public void OnResourceStart()
        {
            int i = DoorManager.registerDoor(-1148826190, new Vector3(82.38156, -1390.476, 29.52609));
            DoorManager.setDoorState(i, false, 0);
            i = DoorManager.registerDoor(868499217, new Vector3(82.38156, -1390.752, 29.52609));
            DoorManager.setDoorState(i, false, 0);

            i = DoorManager.registerDoor(-1922281023, new Vector3(-715.6154, -157.2561, 37.67493));
            DoorManager.setDoorState(i, false, 0);
            i = DoorManager.registerDoor(-1922281023, new Vector3(-716.6755, -155.42, 37.67493));
            DoorManager.setDoorState(i, false, 0);

            i = DoorManager.registerDoor(-1922281023, new Vector3(-1456.201, -233.3682, 50.05648));
            DoorManager.setDoorState(i, false, 0);
            i = DoorManager.registerDoor(-1922281023, new Vector3(-1454.782, -231.7927, 50.05649));
            DoorManager.setDoorState(i, false, 0);

            i = DoorManager.registerDoor(-1922281023, new Vector3(-156.439, -304.4294, 39.99308));
            DoorManager.setDoorState(i, false, 0);
            i = DoorManager.registerDoor(-1922281023, new Vector3(-157.1293, -306.4341, 39.99308));
            DoorManager.setDoorState(i, false, 0);

            i = DoorManager.registerDoor(1780022985, new Vector3(-1201.435, -776.8566, 17.99184));
            DoorManager.setDoorState(i, false, 0);

            i = DoorManager.registerDoor(1780022985, new Vector3(127.8201, -211.8274, 55.22751));
            DoorManager.setDoorState(i, false, 0);

            i = DoorManager.registerDoor(1780022985, new Vector3(617.2458, 2751.022, 42.75777));
            DoorManager.setDoorState(i, false, 0);

            i = DoorManager.registerDoor(1780022985, new Vector3(-3167.75, 1055.536, 21.53288));
            DoorManager.setDoorState(i, false, 0);

            i = DoorManager.registerDoor(-1922281023, new Vector3(-1456.201, -233.3682, 50.05648));
            DoorManager.setDoorState(i, false, 0);
            i = DoorManager.registerDoor(-1922281023, new Vector3(-1454.782, -231.7927, 50.05649));
            DoorManager.setDoorState(i, false, 0);

            i = DoorManager.registerDoor(-1148826190, new Vector3(82.38156, -1390.476, 29.52609));
            DoorManager.setDoorState(i, false, 0);
            i = DoorManager.registerDoor(868499217, new Vector3(82.38156, -1390.752, 29.52609));
            DoorManager.setDoorState(i, false, 0);

            i = DoorManager.registerDoor(1780022985, new Vector3(617.2458, 2751.022, 42.75777));
            DoorManager.setDoorState(i, false, 0);

            i = DoorManager.registerDoor(1780022985, new Vector3(-3167.75, 1055.536, 21.53288));
            DoorManager.setDoorState(i, false, 0);

            // Blaine County Bank
            i = DoorManager.registerDoor(-353187150, new Vector3(-111.48, 6463.94, 31.98499));
            DoorManager.setDoorState(i, false, 0);
            i = DoorManager.registerDoor(-1666470363, new Vector3(-109.65, 6462.11, 31.98499));
            DoorManager.setDoorState(i, false, 0);

            // Discount store (not accurated)
            i = DoorManager.registerDoor(-1148826190, new Vector3(-1.179008, 6516.862, 31.86891));
            DoorManager.setDoorState(i, false, 0);
            i = DoorManager.registerDoor(868499217, new Vector3(-1.179008, 6516.862, 31.86891));
            DoorManager.setDoorState(i, false, 0);

            // Barber PB
            i = DoorManager.registerDoor(-1844444717, new Vector3(-280.7851, 6232.782, 31.84548));
            DoorManager.setDoorState(i, false, 0);

            // Bob Mulét Barber Shop Door
            i = DoorManager.registerDoor(145369505, new Vector3(-822.4442, -188.3924, 37.81895));
            DoorManager.setDoorState(i, false, 0);
            i = DoorManager.registerDoor(-1663512092, new Vector3(-823.2001, -187.0831, 37.81895));
            DoorManager.setDoorState(i, false, 0);

            // Hair on Hawick Barber Shop Door
            i = DoorManager.registerDoor(-1844444717, new Vector3(-29.86917, -148.1571, 57.22648));
            DoorManager.setDoorState(i, false, 0);

            // O'Sheas Barber Shop Door
            i = DoorManager.registerDoor(-1844444717, new Vector3(1932.952, 3725.154, 32.9944));
            DoorManager.setDoorState(i, false, 0);
        }
    }
}
