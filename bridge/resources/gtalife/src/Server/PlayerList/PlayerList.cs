﻿using GTANetworkAPI;
using System.Collections.Generic;
using gtalife.src.Managers;
using gtalife.src.Player.Utils;
using System.Linq;

namespace gtalife.src.Server.PlayerList
{
    class PlayersList : Script
    {
        public class Player
        {
            public int id;
            public string name;
            public int level;
            public int ping;
        }

        [RemoteEvent("get_players_list")]
        public void GetPlayersList(Client player, object[] arguments)
        {
            var players = NAPI.Pools.GetAllPlayers();
            var playerList = new List<Player>();

            foreach (var p in players)
            {
                if (!p.IsLogged()) continue;

                playerList.Add(new Player { id = PlayeridManager.GetPlayerId(p), name = p.Name, level = src.Player.Data.Character[p].Level, ping = NAPI.Player.GetPlayerPing(p) });
            }

            playerList = playerList.OrderBy(p => p.id).ToList();
            NAPI.ClientEvent.TriggerClientEvent(player, "show_players_list", NAPI.Util.ToJson(playerList));
        }
    }
}
