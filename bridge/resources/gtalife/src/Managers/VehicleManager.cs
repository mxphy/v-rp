﻿using GTANetworkAPI;

namespace gtalife.src.Managers
{
    public class VehicleManager : Script
    {
        public static Vehicle CreateVehicle(VehicleHash model, Vector3 pos, Vector3 rot, int color1, int color2, uint dimension = 0)
        {
            Vehicle veh = NAPI.Vehicle.CreateVehicle(model, pos, rot, color1, color2, dimension: dimension);

            NAPI.Data.SetEntityData(veh, "RESPAWNABLE", true);
            NAPI.Data.SetEntityData(veh, "SPAWN_POS", veh.Position);
            NAPI.Data.SetEntityData(veh, "SPAWN_ROT", veh.Rotation);
            NAPI.Data.SetEntityData(veh, "SPAWN_COLOR1", color1);
            NAPI.Data.SetEntityData(veh, "SPAWN_COLOR2", color2);
            NAPI.Data.SetEntityData(veh, "SPAWN_DIMENSION", dimension);

            return veh;
        }
    }
}
