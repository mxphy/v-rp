﻿using GTANetworkAPI;
using gtalife.src.Admin;
using System.IO;
using System.Linq;

namespace gtalife.src.Gameplay.Buildings
{
    class Commands : Script
    {
        [Command("bcmds")]
        public void CMD_CreateHouse(Client player)
        {
            NAPI.Chat.SendChatMessageToPlayer(player, "!{#a5f413}~~~~~~~~~~~ Comandos Buildings ~~~~~~~~~~~");
            NAPI.Chat.SendChatMessageToPlayer(player, "* /crearbuilding - /setbuildingin - /borrarbuilding");
            NAPI.Chat.SendChatMessageToPlayer(player, "!{#a5f413}~~~~~~~~~~~ Comandos Buildings ~~~~~~~~~~~");
        }

        [Command("crearbuilding")]
        public void CMD_CreateBuilding(Client player, string name, uint bliptype, int blipcolor)
        {
            if (player.IsAdmin())
            {
                if (bliptype < 1 || bliptype > 609) player.SendChatMessage("~r~ERROR: ~w~Blip es inválido.");
                else if (blipcolor < 1 || blipcolor > 85) player.SendChatMessage("~r~ERROR: ~w~Blip color es inválida.");
                else
                {
                    Building new_building = new Building(Main.GetGuid(), name, bliptype, blipcolor, player.Position, new Vector3());
                    new_building.Save();

                    Main.Buildings.Add(new_building);

                    player.SendChatMessage($"~g~ÉXITO: ~w~Creaste el building {name}. Index: ~c~{Main.Buildings.IndexOf(new_building)}~w~.");
                }
            }
            else NAPI.Notification.SendNotificationToPlayer(player, "~r~ERROR: ~w~No tienes permiso.");
        }

        [Command("borrarbuilding")]
        public void CMD_DeleteBuilding(Client player)
        {
            if (player.IsAdmin())
            {
                Building building = Main.Buildings.FirstOrDefault(h => h.ID == player.GetData("InsideBuilding_ID"));
                if (building == null) player.SendChatMessage("~r~ERROR: ~w~No estás dentro de un building.");
                else
                {
                    building.Destroy();
                    Main.Buildings.Remove(building);

                    string building_file = Main.BUILDING_SAVE_DIR + Path.DirectorySeparatorChar + building.ID + ".json";
                    if (File.Exists(building_file)) File.Delete(building_file);

                    player.SendChatMessage($"~g~ÉXITO: ~w~Borraste el building {building.Name}.");
                }
            }
            else NAPI.Notification.SendNotificationToPlayer(player, "~r~ERROR: ~w~No tienes permiso.");
        }

        [Command("setbuildingin")]
        public void CMD_SetBuildingInside(Client player, int index)
        {
            if (player.IsAdmin())
            {
                if (Main.Buildings.ElementAtOrDefault(index) == null) player.SendChatMessage("~r~ERROR: ~w~Index inválido.");
                else
                {
                    uint dimension = 0;
                    if (Main.Buildings[index].Dimension != 0) dimension = Main.Buildings[index].Dimension;
                    else
                    {
                        do
                        {
                            dimension++;
                        }
                        while (Main.Buildings.Count(b => b.Dimension == dimension) > 0);
                    }

                    player.Dimension = dimension;
                    Main.Buildings[index].SetInsideMarker(player.Position);
                    Main.Buildings[index].SetDimension(dimension);
                    Main.Buildings[index].Save(true);

                    player.SendChatMessage($"~g~ÉXITO: ~w~Cambiaste el interior del building {Main.Buildings[index].Name}.");
                }
            }
            else NAPI.Notification.SendNotificationToPlayer(player, "~r~ERROR: ~w~No tienes permiso.");
        }

    }
}
