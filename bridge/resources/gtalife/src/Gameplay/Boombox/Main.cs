﻿using GTANetworkAPI;
using System;
using System.Linq;
using System.Collections.Generic;
using gtalife.src.Player.Utils;
using gtalife.src.Player.Inventory.Extensions;
using gtalife.src.Player.Inventory.Classes;

namespace gtalife.src.Gameplay.Boombox
{
    public class Main : Script
    {
        public static string BOOMBOX_SAVE_DIR = "data/Boombox";
        public static float BOOMBOX_RANGE = 20f;
        public static int SAVE_INTERVAL = 120;

        public static List<Boombox> Boomboxes = new List<Boombox>();

        [RemoteEvent("BoomBox:Select")]
        public void BoomBoxSelect(Client player, object[] arguments)
        {
            Boombox boombox = Boomboxes.FirstOrDefault(b => b.ID == player.GetData("Boombox_ID"));
            if (boombox == null) return;

            player.sendChatAction(boombox.Enabled ? "ha cambiado la estación de radio." : "ha encendido la radio.");

            boombox.Enabled = true;
            boombox.SetGenre((string)arguments[0]);
            boombox.Url = (string)arguments[1];

            boombox.UpdateForAll();
        }

        [RemoteEvent("BoomBox:TurnOff")]
        public void BoomBoxTurnOff(Client player, object[] arguments)
        {
            Boombox boombox = Boomboxes.FirstOrDefault(b => b.ID == player.GetData("Boombox_ID"));
            if (boombox == null) return;

            player.sendChatAction("ha apagado la radio.");

            boombox.Enabled = false;
            boombox.SetGenre("");
            boombox.Url = "";

            boombox.UpdateForAll();
        }

        [RemoteEvent("BoomBox:Take")]
        public void BoomBoxTake(Client player, object[] arguments)
        {
            Boombox boombox = Boomboxes.FirstOrDefault(b => b.ID == player.GetData("Boombox_ID"));
            if (boombox == null) return;

            player.sendChatAction("ha tomado la radio.");

            boombox.Enabled = false;
            boombox.SetGenre("");
            boombox.Url = "";
            boombox.UpdateForAll();

            Boomboxes.Remove(boombox);
            boombox.Destroy();

            player.giveItem(ItemID.Boombox, 1);
        }

        #region Methods
        public static Guid GetGuid()
        {
            Guid new_guid;

            do
            {
                new_guid = Guid.NewGuid();
            } while (Boomboxes.Count(h => h.ID == new_guid) > 0);

            return new_guid;
        }
        #endregion
    }
}
