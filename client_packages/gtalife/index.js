// Player
require('./gtalife/player/auth/login.js');
require('./gtalife/player/character/selection.js');
require('./gtalife/player/character/creation.js');
require('./gtalife/player/list/index.js');

// Animation
require('./gtalife/player/animation/playanim.js');
require('./gtalife/player/animation/facialmood.js');

// Admin
require('./gtalife/admin/spawner/index.js');
require('./gtalife/admin/interiorBrowser/index.js');

// Job
require('./gtalife/job/index.js');

// Vehicle
require('./gtalife/vehicle/engine.js');
require('./gtalife/vehicle/doors.js');

// Dealership
require('./gtalife/dealership/index.js');

// School
require('./gtalife/school/index.js');
require('./gtalife/school/tests/driving.js');

// Inventory
require('./gtalife/inventory/index.js');
require('./gtalife/inventory/itemDetector.js');

// Business
require('./gtalife/business/index.js');
require('./gtalife/business/store.js');
require('./gtalife/business/advertisement.js');
require('./gtalife/business/rent.js');
require('./gtalife/business/bar.js');

// UI
require('./gtalife/ui/money.js');
require('./gtalife/ui/watermark.js');
require('./gtalife/ui/compass.js');

// Gang
require('./gtalife/gang/zone.js');

// Utils
require('./gtalife/util/browser.js');